﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class SimulationManager : MonoBehaviour {

	public WorldObjectsManagerScript worldObjectsManagerScript;
	public GUIManagerScript guiManagerScript;
	public PreGameSettingManager preGameManagerScript;
	public TrafficLightSequenceManager trafficLightSequenceManager;
	public SimulationStats simStat;

	//Finds all the spawners and assigns them to their correct lists.
	public void setUp() {

		worldObjectsManagerScript = GameObject.Find ("WorldObjectsManager").GetComponent<WorldObjectsManagerScript> ();
		guiManagerScript = GameObject.Find ("GUIManager").GetComponent<GUIManagerScript> ();
		preGameManagerScript = GameObject.Find ("GameSettingsManager").GetComponent<PreGameSettingManager> ();
		trafficLightSequenceManager = GameObject.Find ("TrafficLightSequenceManager").GetComponent<TrafficLightSequenceManager> ();
		simStat = GameObject.Find ("SimulationManager").GetComponent<SimulationStats> ();


	}

	public void startSim(){
		resetSim ();
		TurnOnAllSpawners ();
		startLightSequencing ();
	}

	public void finishSim(){
		TurnOffAllSpawners ();
		trafficLightSequenceManager.stopLights ();
		deleteAllCars ();
		//simStat.stopRecording ();
	}

	public void resetSim(){
		TurnOffAllSpawners ();
		trafficLightSequenceManager.stopLights ();
		deleteAllCars ();
		simStat.resetStats ();
	}

	public void TurnOnAllSpawners() { // should not be used
		if(preGameManagerScript.NSpawnOn){
			foreach(GameObject spawner in preGameManagerScript.NSpawners){
				spawner.GetComponent<VehicleSpawn> ().TurnOn();
			}
		}
		if (preGameManagerScript.ESpawnOn) {
			foreach (GameObject spawner in preGameManagerScript.ESpawners) {
				spawner.GetComponent<VehicleSpawn> ().TurnOn ();
			}
		}
		if (preGameManagerScript.SSpawnOn) {
			foreach (GameObject spawner in preGameManagerScript.SSpawners) {
				spawner.GetComponent<VehicleSpawn> ().TurnOn ();
			}
		}
		if (preGameManagerScript.WSpawnOn) {
			foreach (GameObject spawner in preGameManagerScript.WSpawners) {
				spawner.GetComponent<VehicleSpawn> ().TurnOn ();
			}
		}
	}

	public void TurnOffAllSpawners() { // should not be used

		foreach(GameObject spawner in preGameManagerScript.NSpawners){
			spawner.GetComponent<VehicleSpawn> ().TurnOff();
		}

		foreach(GameObject spawner in preGameManagerScript.ESpawners){
			spawner.GetComponent<VehicleSpawn> ().TurnOff();
		}

		foreach(GameObject spawner in preGameManagerScript.SSpawners){
			spawner.GetComponent<VehicleSpawn> ().TurnOff();
		}

		foreach(GameObject spawner in preGameManagerScript.WSpawners){
			spawner.GetComponent<VehicleSpawn> ().TurnOff();
		}
	}

	public void setSpeed(){
		Time.timeScale =  guiManagerScript.simulation.transform.Find ("Panel").Find ("Speed").GetComponent<Slider> ().value;
	}

	public void startLightSequencing (){
		preGameManagerScript.trafficLightSequenceManager.startLights ();
	}

	//public void resetSimStats(){}
	//public void startSimStats(){}

	public void deleteAllCars(){
		worldObjectsManagerScript.deleteCars ();
	}
}