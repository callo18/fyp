﻿using UnityEngine;
using System.Collections;

public class MouseManagerScript : MonoBehaviour {

	private GameObject selectedObject;
	public Material selected;
	public Material destination;
	public Material idle;
	public Material white;

	private bool straight, left, right;
	private bool choseDestination;

	public GameObject SelectedObject {
		get {
			return selectedObject;
		}
		set {
			selectedObject = value;
		}
	}

	// Use this for initialization
	void Start () {
		
	}
	// Update is called once per frame
	void Update () {
		SelectChosenDecisionPoint ();
		if (selectedObject != null) {
			SelectChosenDestinationPoint ();
		}
	}

	void SelectChosenDecisionPoint(){

		Camera theCamera = Camera.main;

		if (Input.GetMouseButtonDown (0)) {
			Ray ray = theCamera.ScreenPointToRay (Input.mousePosition);
			RaycastHit hitInfo;

			if (Physics.Raycast (ray, out hitInfo)) {
				//Debug.Log ("The hit object is " + hitInfo.collider.gameObject.name);
				if (selectedObject!=null && selectedObject.Equals(hitInfo.collider.gameObject) ){

					DeselectSelected ();

				}
					
				else if (hitInfo.collider.gameObject.tag.Equals("DecisionPointArrow")) { // 9 equals the editable layer

					DeselectSelected ();
					//Select New
					selectedObject = hitInfo.collider.gameObject;
			
					//Set Colours
					selectedObject.GetComponentInParent<Renderer> ().material = idle;
					selectedObject.transform.parent.GetComponentInParent<Renderer> ().material = selected;

					//Find Possible destinations
					if (selectedObject.name == "Straight") {
						straight = true;
						left = false;
						right = false;
						selectedObject.GetComponentInParent<DecisionPoint> ().highlightSDestinationPoints ();
					} else if (selectedObject.name == "Right") {
						straight = false;
						left = false;
						right = true;
						selectedObject.GetComponentInParent<DecisionPoint> ().highlightRDestinationPoints ();
					} else if (selectedObject.name == "Left") {
						straight = false;
						left = true;
						right = false;
						selectedObject.GetComponentInParent<DecisionPoint> ().highlightLDestinationPoints ();
					}
				}
			}
		} 
	}

	void SelectChosenDestinationPoint(){

		Camera theCamera = Camera.main;

		if (Input.GetMouseButtonDown (0)) {

			Ray ray = theCamera.ScreenPointToRay (Input.mousePosition);
			RaycastHit hitInfo;

			if (Physics.Raycast (ray, out hitInfo)) {
				//Debug.Log ("The hit object is " + hitInfo.collider.gameObject.name);
				if (hitInfo.collider.gameObject.tag.Equals("DestinationPoint")) { 
					choseDestination = true;
					if (straight) {
						selectedObject.GetComponentInParent<DecisionPoint> ().sdestination = hitInfo.collider.gameObject;
					} else if (left) {
						selectedObject.GetComponentInParent<DecisionPoint> ().ldestination = hitInfo.collider.gameObject;
					} else if (right) {
						selectedObject.GetComponentInParent<DecisionPoint> ().rdestination = hitInfo.collider.gameObject;
					}
					selectedObject.GetComponentInParent<Renderer> ().material = destination;
					DeselectSelected ();
				}
			}
		} 
	}

	public void DeselectSelected(){
		if(selectedObject != null){

			selectedObject.GetComponentInParent<DecisionPoint> ().unhighlightSDestinationPoints ();
			selectedObject.GetComponentInParent<DecisionPoint> ().unhighlightLDestinationPoints ();
			selectedObject.GetComponentInParent<DecisionPoint> ().unhighlightRDestinationPoints ();
			selectedObject.transform.parent.GetComponentInParent<Renderer> ().material = idle;
			if (!choseDestination) {
				print ("Done");
				selectedObject.GetComponentInParent<Renderer> ().material = white;
				if (straight) {
					selectedObject.GetComponentInParent<DecisionPoint> ().sdestination = null;
				} else if (left) {
					selectedObject.GetComponentInParent<DecisionPoint> ().ldestination = null;
				} else if (right) {
					selectedObject.GetComponentInParent<DecisionPoint> ().rdestination = null;
				}
			}
			choseDestination = false;
			selectedObject = null;
		}
	}
}
