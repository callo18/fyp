﻿using UnityEngine;
using System.Collections;

public class VehicleCollector : MonoBehaviour {
	SimulationStats simStat;
	// Use this for initialization
	void Start () {
		simStat= GameObject.Find ("SimulationManager").GetComponent<SimulationStats> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
		
	void OnTriggerEnter(Collider other) {
		collectCar (other);
	}

	void collectCar(Collider other){
		simStat.collectCar ();
		Destroy(other.gameObject);
	}
}
