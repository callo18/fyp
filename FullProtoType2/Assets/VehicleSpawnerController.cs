﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class VehicleSpawnerController : MonoBehaviour {

	private GameObject junctionPiece;
	public List<GameObject> NSpawners;
	public List<GameObject> ESpawners;
	public List<GameObject> SSpawners;
	public List<GameObject> WSpawners;

	public float NSpawnSpeed,ESpawnSpeed,SSpawnSpeed,WSpawnSpeed;
	public bool NSpawnOn,ESpawnOn,SSpawnOn,WSpawnOn;

	public GameObject settingsUI;


	//Finds all the spawners and assigns them to their correct lists.
	public void SetUpSpawners() {
		
		junctionPiece = GameObject.Find ("Junction(Clone)");
		NSpawners = junctionPiece.GetComponent<JunctionPiece> ().NSpawners;
		ESpawners = junctionPiece.GetComponent<JunctionPiece> ().ESpawners;
		SSpawners = junctionPiece.GetComponent<JunctionPiece> ().SSpawners;
		WSpawners = junctionPiece.GetComponent<JunctionPiece> ().WSpawners;

	}








	public void TurnOnAllSpawners() { // should not be used
		if(NSpawnOn){
			foreach(GameObject spawner in NSpawners){
				spawner.GetComponent<VehicleSpawn> ().TurnOn();
			}
		}
		if (ESpawnOn) {
			foreach (GameObject spawner in ESpawners) {
				spawner.GetComponent<VehicleSpawn> ().TurnOn ();
			}
		}
		if (SSpawnOn) {
			foreach (GameObject spawner in SSpawners) {
				spawner.GetComponent<VehicleSpawn> ().TurnOn ();
			}
		}
		if (WSpawnOn) {
			foreach (GameObject spawner in WSpawners) {
				spawner.GetComponent<VehicleSpawn> ().TurnOn ();
			}
		}
	}

	public void TurnOffAllSpawners() { // should not be used

		foreach(GameObject spawner in NSpawners){
			spawner.GetComponent<VehicleSpawn> ().TurnOff();
		}

		foreach(GameObject spawner in ESpawners){
			spawner.GetComponent<VehicleSpawn> ().TurnOff();
		}

		foreach(GameObject spawner in SSpawners){
			spawner.GetComponent<VehicleSpawn> ().TurnOff();
		}

		foreach(GameObject spawner in WSpawners){
			spawner.GetComponent<VehicleSpawn> ().TurnOff();
		}
	}

	public void applySpawnerSettings(){

		getSettings ();
		setSettingsForEachSpawner ();
		foreach(GameObject spawner in NSpawners){
			spawner.GetComponent<VehicleSpawn> ().setSpawnRate(NSpawnSpeed);
		}

		foreach(GameObject spawner in ESpawners){
			spawner.GetComponent<VehicleSpawn> ().setSpawnRate(ESpawnSpeed);
		}

		foreach(GameObject spawner in SSpawners){
			spawner.GetComponent<VehicleSpawn> ().setSpawnRate(SSpawnSpeed);
		}

		foreach(GameObject spawner in WSpawners){
			spawner.GetComponent<VehicleSpawn> ().setSpawnRate(WSpawnSpeed);
		}
	}

	public void getSettings(){
		
		NSpawnSpeed = settingsUI.transform.Find ("N").Find ("Slider").GetComponent<Slider> ().value;
		ESpawnSpeed = settingsUI.transform.Find ("E").Find ("Slider").GetComponent<Slider> ().value;
		SSpawnSpeed = settingsUI.transform.Find ("S").Find ("Slider").GetComponent<Slider> ().value;
		WSpawnSpeed = settingsUI.transform.Find ("W").Find ("Slider").GetComponent<Slider> ().value;

		NSpawnOn = settingsUI.transform.Find ("N").Find ("Toggle").GetComponent<Toggle> ().isOn;
		ESpawnOn = settingsUI.transform.Find ("E").Find ("Toggle").GetComponent<Toggle> ().isOn;
		SSpawnOn = settingsUI.transform.Find ("S").Find ("Toggle").GetComponent<Toggle> ().isOn;
		WSpawnOn = settingsUI.transform.Find ("W").Find ("Toggle").GetComponent<Toggle> ().isOn;

	}

	public void setSettingsForEachSpawner(){
		
		foreach (GameObject spawner in NSpawners) {
			spawner.GetComponent<VehicleSpawn> ().setSpawnRate (NSpawnSpeed);
		}

		foreach (GameObject spawner in ESpawners) {
			spawner.GetComponent<VehicleSpawn> ().setSpawnRate (ESpawnSpeed);
		}

		foreach (GameObject spawner in SSpawners) {
			spawner.GetComponent<VehicleSpawn> ().setSpawnRate (SSpawnSpeed);
		}

		foreach (GameObject spawner in WSpawners) {
			spawner.GetComponent<VehicleSpawn> ().setSpawnRate (WSpawnSpeed);
		}

	}
}
