﻿using UnityEngine;
using System.Collections;

public class GUIManagerScript : MonoBehaviour {

	public GameObject intro,junctionBuilding,editingLanes,preGameSettings,simulation,postGameReport,GameStateGUI; 

	// Use this for initialization
	void Start () {
	}

	// Update is called once per frame
	void Update () {
	}

	public void IntroScreen(){
		SetAllFalse ();
		intro.SetActive (true);
	}

	public void JunctionBuildingScreen(){
		SetAllFalse ();
		junctionBuilding.SetActive (true);
	}

	public void EditingLanesScreen(){
		SetAllFalse ();
		editingLanes.SetActive (true);
		GameStateGUI.SetActive(true);
	}

	public void GameSettingScreen(){
		SetAllFalse ();
		preGameSettings.SetActive (true);
		GameStateGUI.SetActive(true);
	}

	public void SimulationScreen(){
		SetAllFalse ();
		simulation.SetActive (true);
		GameStateGUI.SetActive(true);
	}

	public void PostGameReportScreen(){
		SetAllFalse ();
		postGameReport.SetActive (true);
	}

	private void SetAllFalse(){
		
		intro.SetActive (false);
		junctionBuilding.SetActive (false);
		editingLanes.SetActive (false);
		preGameSettings.SetActive (false);
		simulation.SetActive (false);
		//postGameReport.SetActive (false);
		GameStateGUI.SetActive(false);
	}

}
