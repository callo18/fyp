﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class VehicleSpawn : MonoBehaviour {

	public GameObject [] vehicles = new GameObject [10];
	public bool off = true;
	private bool PossibleDestinations;
	public float spawnRateMax;
	public float spawnRate;
	public Transform DesicionPoint;
	public int speedLimit;
	public SimulationStats simStat;
	public WorldObjectsManagerScript worldObjectsMan;
	public List<string> decisionPointsPossibleDestinationsDirections;
	public Material[] roadSigns;

	void Start(){

		worldObjectsMan = GameObject.Find ("WorldObjectsManager").GetComponent<WorldObjectsManagerScript> ();
		simStat= GameObject.Find ("SimulationManager").GetComponent<SimulationStats> ();
	}

	IEnumerator SpawnVehicle()
	{
		DesicionPoint = this.transform.parent.Find("DecisionPoint");
		spawnRateMax = 10;
		off = false;
		while(true)
		{
			GameObject vehicle = (GameObject)Instantiate(vehicles[Random.Range (0, 10)], this.transform.position, this.transform.rotation);
			vehicle.transform.parent = this.transform.parent;
			vehicle.GetComponent<VehicleMovement> ().setUpCar ();
			worldObjectsMan.addCar (vehicle);
			simStat.addCar ();
			yield return new WaitForSeconds(Random.Range(.5f, (spawnRateMax - spawnRate) + 1f));

		}
	}

	public void setSpawnRate(float x){
		spawnRate = x;
	}

	public void TurnOn(){
		if (PossibleDestinations) {
			StartCoroutine (SpawnVehicle ());
		}
	}

	public void TurnOff(){
		StopAllCoroutines ();
	}

	// Lane related mathods

	public void checkForPossibleDestination(){
		if (this.transform.parent.Find ("DecisionPoint").GetComponent<DecisionPoint> ().validsDestinations.Count == 0) {
			PossibleDestinations = false;
			transform.Find ("RoadBlock").gameObject.SetActive (true);
		} else {
			PossibleDestinations = true;
			transform.Find ("RoadBlock").gameObject.SetActive (false);
		}
	}

	public void setSpeedLimit(int x){
		speedLimit = x;
	}

	public void setUpRoadSign(){
		decisionPointsPossibleDestinationsDirections = transform.parent.FindChild ("DecisionPoint").GetComponent<DecisionPoint> ().validsDestinationsDirections;

		if (decisionPointsPossibleDestinationsDirections.Count == 0) {
			transform.parent.Find ("RoadSigns").Find ("Plane").gameObject.GetComponentInParent<Renderer> ().material = roadSigns [0];
		}
		//three directions
		else if (decisionPointsPossibleDestinationsDirections.Count == 3) {
			transform.parent.Find ("RoadSigns").Find ("Plane").gameObject.GetComponentInParent<Renderer> ().material = roadSigns [7];
		}
		//two directions
		else if (decisionPointsPossibleDestinationsDirections.Count == 2) {
			if (decisionPointsPossibleDestinationsDirections.Contains ("left") && decisionPointsPossibleDestinationsDirections.Contains ("right")) {
				transform.parent.Find ("RoadSigns").Find ("Plane").gameObject.GetComponentInParent<Renderer> ().material = roadSigns [5];
			} else if (decisionPointsPossibleDestinationsDirections.Contains ("left") && decisionPointsPossibleDestinationsDirections.Contains ("straight")) {
				transform.parent.Find ("RoadSigns").Find ("Plane").gameObject.GetComponentInParent<Renderer> ().material = roadSigns [4];
			} else if (decisionPointsPossibleDestinationsDirections.Contains ("straight") && decisionPointsPossibleDestinationsDirections.Contains ("right")) {
				transform.parent.Find ("RoadSigns").Find ("Plane").gameObject.GetComponentInParent<Renderer> ().material = roadSigns [6];
			}

		}
		//one direction
		else if (decisionPointsPossibleDestinationsDirections.Count == 1) {
			if (decisionPointsPossibleDestinationsDirections.Contains ("left")) {
				transform.parent.Find ("RoadSigns").Find ("Plane").gameObject.GetComponentInParent<Renderer> ().material = roadSigns [1];
			} else if (decisionPointsPossibleDestinationsDirections.Contains ("right")) {
				transform.parent.Find ("RoadSigns").Find ("Plane").gameObject.GetComponentInParent<Renderer> ().material = roadSigns [3];
			} else if (decisionPointsPossibleDestinationsDirections.Contains ("straight")) {
				transform.parent.Find ("RoadSigns").Find ("Plane").gameObject.GetComponentInParent<Renderer> ().material = roadSigns [2];
			}
		}
	}
}
