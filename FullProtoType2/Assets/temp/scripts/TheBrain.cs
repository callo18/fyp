﻿using UnityEngine;
using System.Collections;

public class TheBrain : MonoBehaviour {
	public TrafficLightsBehaviour [] Lights = new TrafficLightsBehaviour[4];
	// Use this for initialization
	void Start () {
		StartCoroutine(ChangeLights());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator ChangeLights()
	{
		while(true)
		{
			for (int i = 0; i < Lights.Length; i++) {
				Lights [i].redLight = !Lights [i].redLight;
			}
			yield return new WaitForSeconds (3);
		}
	}
}
