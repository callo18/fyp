﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TrafficLightsBehaviour : MonoBehaviour {

	public List<GameObject> Lights;

	public bool redLight = true;
	public bool greenLeft = false;
	public bool greenStraight = false;
	public bool greenRight = false;

	public void makeRed(){
		
		redLight = true;
		greenLeft = false;
		greenStraight = false;
		greenRight = false;
	}

	public void makeGreen(){

		redLight = false;
		greenLeft = true;
		greenStraight = true;
		greenRight = true;
	}

	public void makeAmber (){



		if(!redLight){

			foreach (GameObject light in Lights) {
				light.transform.Find("lights").Find ("Green").GetComponent<Light> ().enabled = false;
				light.transform.Find("lights").Find ("Red").GetComponent<Light> ().enabled = false;
				light.transform.Find("lights").Find ("Yellow").GetComponent<Light> ().enabled = true;
			}
		
			makeRed ();
		}
	}

	public void changeLights(){
		if (redLight) {
			foreach (GameObject light in Lights) {
				light.transform.Find("lights").Find ("Green").GetComponent<Light> ().enabled = false;
				light.transform.Find("lights").Find ("Red").GetComponent<Light> ().enabled = true;
				light.transform.Find("lights").Find ("Yellow").GetComponent<Light> ().enabled = false;
			}
		} else {
			if (greenStraight) {
				foreach (GameObject light in Lights) {
					light.transform.Find("lights").Find ("Green").GetComponent<Light> ().enabled = true;
					light.transform.Find("lights").Find ("Red").GetComponent<Light> ().enabled = false;
					light.transform.Find("lights").Find ("Yellow").GetComponent<Light> ().enabled = false;
				}
			} else {
				if (greenLeft && greenRight) {
					Lights[1].transform.Find("lights").Find ("Green").GetComponent<Light> ().enabled = true;
					Lights[1].transform.Find("lights").Find ("Red").GetComponent<Light> ().enabled = false;
					Lights[1].transform.Find("lights").Find ("Yellow").GetComponent<Light> ().enabled = false;

					Lights[0].transform.Find("lights").Find ("Green").GetComponent<Light> ().enabled = true;
					Lights[0].transform.Find("lights").Find ("Red").GetComponent<Light> ().enabled = false;
					Lights[0].transform.Find("lights").Find ("Yellow").GetComponent<Light> ().enabled = false;
				}
				else if (greenRight) {
					Lights[0].transform.Find("lights").Find ("Green").GetComponent<Light> ().enabled = true;
					Lights[0].transform.Find("lights").Find ("Red").GetComponent<Light> ().enabled = false;
					Lights[0].transform.Find("lights").Find ("Yellow").GetComponent<Light> ().enabled = false;

					Lights[1].transform.Find("lights").Find ("Green").GetComponent<Light> ().enabled = false;
					Lights[1].transform.Find("lights").Find ("Red").GetComponent<Light> ().enabled = true;
					Lights[1].transform.Find("lights").Find ("Yellow").GetComponent<Light> ().enabled = false;
				}
				else if (greenLeft) {
					Lights[0].transform.Find("lights").Find ("Green").GetComponent<Light> ().enabled = false;
					Lights[0].transform.Find("lights").Find ("Red").GetComponent<Light> ().enabled = true;
					Lights[0].transform.Find("lights").Find ("Yellow").GetComponent<Light> ().enabled = false;

					Lights[1].transform.Find("lights").Find ("Green").GetComponent<Light> ().enabled = true;
					Lights[1].transform.Find("lights").Find ("Red").GetComponent<Light> ().enabled = false;
					Lights[1].transform.Find("lights").Find ("Yellow").GetComponent<Light> ().enabled = false;
				}
			}
		}
	}
}
