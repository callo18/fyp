﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class VehicleMovement : MonoBehaviour { 

	private float trafficLightDist = 1.2f;
	bool moving = true;
	public Transform Destination0Transform,Destination1Transform,Destination8Transform,Destination9Transform;
	public Vector3 Destination0,Destination1,Destination2,Destination3,Destination4,Destination5,Destination6,Destination7,Destination8,Destination9;
	public int StartingDestination = 1;
	public List<GameObject> decisionPointsPossibleDestinations;
	public List<string> decisionPointsPossibleDestinationsDirections;

	public List<Vector3> Destinations;
	public Vector3 currentDestination;
	//N = 0 E = 1 S = 2 W = 3
	public int carDirectionFrom;
	public bool vertical = true;
	public string chosenDirection;
	public float distanceFromPointLeniance = 1f;
	//
	public GameObject mySpawner;
	public int speedLimit;
	
	// Update is called once per frame
	void FixedUpdate () {
		vehicleMove ();
		checkSurroundings ();
		checkForNewDestination ();
		//debugLines ();
	}
		
	void checkSurroundings(){
		if (this.CheckForTrafficL () || this.CheckForVehicle () ) {
			moving = false;
		} else {
			moving = true;
		}
	}

	void vehicleMove(){
		if (moving) {
			Vector3 targetDir = currentDestination - transform.position;
			float step = 20f * Time.deltaTime;
			Vector3 newDir = Vector3.RotateTowards (transform.forward, targetDir, step, 0.0F);
			transform.rotation = Quaternion.LookRotation (newDir);
			transform.Translate(Vector3.forward * Time.deltaTime * (speedLimit/15));
		}
	}

	void checkForNewDestination(){
		if (((transform.position.x <= (currentDestination.x + distanceFromPointLeniance))&&(transform.position.x >= (currentDestination.x - distanceFromPointLeniance)))&&
			((transform.position.y <= (currentDestination.y + distanceFromPointLeniance))&&(transform.position.y >= (currentDestination.y - distanceFromPointLeniance)))&&
			((transform.position.z <= (currentDestination.z + distanceFromPointLeniance))&&(transform.position.z >= (currentDestination.z - distanceFromPointLeniance))))
		{
			StartingDestination += 1;
			currentDestination = Destinations [StartingDestination];
		}
	}

	bool CheckForTrafficL() 
	{
		Vector3 fwd = transform.TransformDirection(Vector3.forward);
		RaycastHit hit;
		if (Physics.Raycast (transform.position, fwd, out hit, trafficLightDist)) {
			if(hit.collider.tag.Equals("TrafficLights")){
				TrafficLightsBehaviour tlb = hit.collider.transform.parent.GetComponent<TrafficLightsBehaviour> ();
				if (chosenDirection == "straight" && tlb.greenStraight) {
					return false;
				} else if (chosenDirection == "left" && tlb.greenLeft) {
						return false;
				}else if (chosenDirection == "right" && tlb.greenRight) {
					return false;
				} else
					return true;
			}
			return false;
		} else
			return false;
	}
	bool CheckForVehicle() 
	{
		Vector3 fwd = transform.TransformDirection(Vector3.forward);
		RaycastHit hit;
		if (Physics.Raycast (transform.position, fwd, out hit, 1)) {
			if(hit.collider.tag.Equals("Vehicle")){
				return true;
			}
			return false;
		} else
			return false;
	}

	public void setUpCar(){
		mySpawner = transform.parent.FindChild ("VehicleSpawner").gameObject;
		FindDestinations ();
		getSpeedLimit ();
	}

	public void getSpeedLimit (){

		speedLimit = mySpawner.GetComponent<VehicleSpawn> ().speedLimit;
	}

	public void FindDestinations(){
		
		decideSpawnDirection ();

		Destination0Transform = mySpawner.transform;
		Destination0 = Destination0Transform.position;

		Destination1Transform = transform.parent.FindChild ("DecisionPoint").transform;
		Destination1 = Destination1Transform.position;

		decisionPointsPossibleDestinations = transform.parent.FindChild ("DecisionPoint").GetComponent<DecisionPoint> ().validsDestinations;
		decisionPointsPossibleDestinationsDirections = transform.parent.FindChild ("DecisionPoint").GetComponent<DecisionPoint> ().validsDestinationsDirections;

		if (decisionPointsPossibleDestinations.Count == 0) {
			
			Destination2 = Destination1;
			Destination3 = Destination1;
			Destination4 = Destination1;
			Destination5 = Destination1;
			Destination6 = Destination1;
			Destination7 = Destination1;
			Destination8 = Destination1;
			Destination9 = Destination1;

			Destinations.Add (Destination0);
			Destinations.Add (Destination1);
			Destinations.Add (Destination2);
			Destinations.Add (Destination3);
			Destinations.Add (Destination4);
			Destinations.Add (Destination5);
			Destinations.Add (Destination6);
			Destinations.Add (Destination7);
			Destinations.Add (Destination8);
			Destinations.Add (Destination9);

			currentDestination = Destinations [StartingDestination];

		} else {
			
			int x = Random.Range (0, decisionPointsPossibleDestinations.Count);
			chosenDirection = decisionPointsPossibleDestinationsDirections [x];
			Destination8Transform = decisionPointsPossibleDestinations [x].transform;
			Destination8 = Destination8Transform.position;

			Destination9Transform = Destination8Transform.GetComponent<DestinationPoint> ().Collector.transform;
			Destination9 = Destination9Transform.position;

			bool xAdd = true;
			bool zAdd = true;
			if (Mathf.Abs ((Destination1.x + .01f) - Destination8.x) <= Mathf.Abs ((Destination1.x - .01f) - Destination8.x)) {
				xAdd = true;
			}
			else{
				xAdd = false;
			}
			if (Mathf.Abs ((Destination1.z + .01f) - Destination8.z) <= Mathf.Abs ((Destination1.z - .01f) - Destination8.z)) {
				zAdd = true;
			}
			else{
				zAdd = false;
			}
			if (chosenDirection != "straight") {
				if (vertical) {
				
					if (xAdd && zAdd) {
					
						Destination2 = new Vector3 (Destination1.x + (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 1), .1f, (Destination1.z + ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 10));

						Destination3 = new Vector3 (Destination1.x + (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 2), .1f, (Destination1.z + ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 12));

						Destination4 = new Vector3 (Destination1.x + (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 3), .1f, (Destination1.z + ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 15));

						Destination5 = new Vector3 (Destination1.x + (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 5), .1f, (Destination1.z + ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 17));

						Destination6 = new Vector3 (Destination1.x + (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 7), .1f, (Destination1.z + ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 18));

						Destination7 = new Vector3 (Destination1.x + (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 9), .1f, (Destination1.z + ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 19));
				
					} else if (xAdd && !zAdd) {
					
						Destination2 = new Vector3 (Destination1.x + (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 1), .1f, (Destination1.z - ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 10));

						Destination3 = new Vector3 (Destination1.x + (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 2), .1f, (Destination1.z - ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 12));

						Destination4 = new Vector3 (Destination1.x + (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 3), .1f, (Destination1.z - ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 15));

						Destination5 = new Vector3 (Destination1.x + (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 5), .1f, (Destination1.z - ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 17));

						Destination6 = new Vector3 (Destination1.x + (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 7), .1f, (Destination1.z - ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 18));

						Destination7 = new Vector3 (Destination1.x + (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 9), .1f, (Destination1.z - ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 19));
				
					} else if (!xAdd && !zAdd) {
					
						Destination2 = new Vector3 (Destination1.x - (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 1), .1f, (Destination1.z - ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 10));

						Destination3 = new Vector3 (Destination1.x - (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 2), .1f, (Destination1.z - ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 12));

						Destination4 = new Vector3 (Destination1.x - (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 3), .1f, (Destination1.z - ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 15));

						Destination5 = new Vector3 (Destination1.x - (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 5), .1f, (Destination1.z - ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 17));

						Destination6 = new Vector3 (Destination1.x - (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 7), .1f, (Destination1.z - ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 18));

						Destination7 = new Vector3 (Destination1.x - (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 9), .1f, (Destination1.z - ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 19));
				
					} else if (!xAdd && zAdd) {
					
						Destination2 = new Vector3 (Destination1.x - (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 1), .1f, (Destination1.z + ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 10));

						Destination3 = new Vector3 (Destination1.x - (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 2), .1f, (Destination1.z + ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 12));

						Destination4 = new Vector3 (Destination1.x - (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 3), .1f, (Destination1.z + ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 15));

						Destination5 = new Vector3 (Destination1.x - (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 5), .1f, (Destination1.z + ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 17));

						Destination6 = new Vector3 (Destination1.x - (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 7), .1f, (Destination1.z + ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 18));

						Destination7 = new Vector3 (Destination1.x - (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 9), .1f, (Destination1.z + ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 19));
					}
				} 
			//-------------------------------------------------------------------------------------------------------------------------------------------------------------
			else {

					if (xAdd && zAdd) {

						Destination2 = new Vector3 ((Destination1.x + ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 10), .1f, (Destination1.z + (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 1)));

						Destination3 = new Vector3 ((Destination1.x + ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 12), .1f, (Destination1.z + (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 2)));

						Destination4 = new Vector3 ((Destination1.x + ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 15), .1f, (Destination1.z + (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 3)));

						Destination5 = new Vector3 ((Destination1.x + ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 17), .1f, (Destination1.z + (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 5)));

						Destination6 = new Vector3 ((Destination1.x + ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 18), .1f, (Destination1.z + (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 7)));

						Destination7 = new Vector3 ((Destination1.x + ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 19), .1f, (Destination1.z + (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 9)));

					} else if (xAdd && !zAdd) {

						Destination2 = new Vector3 ((Destination1.x + ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 10), .1f, (Destination1.z - (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 1)));

						Destination3 = new Vector3 ((Destination1.x + ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 12), .1f, (Destination1.z - (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 2)));

						Destination4 = new Vector3 ((Destination1.x + ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 15), .1f, (Destination1.z - (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 3)));

						Destination5 = new Vector3 ((Destination1.x + ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 17), .1f, (Destination1.z - (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 5)));

						Destination6 = new Vector3 ((Destination1.x + ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 18), .1f, (Destination1.z - (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 7)));

						Destination7 = new Vector3 ((Destination1.x + ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 19), .1f, (Destination1.z - (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 9)));
					} else if (!xAdd && !zAdd) {

						Destination2 = new Vector3 ((Destination1.x - ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 10), .1f, (Destination1.z - (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 1)));

						Destination3 = new Vector3 ((Destination1.x - ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 12), .1f, (Destination1.z - (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 2)));

						Destination4 = new Vector3 ((Destination1.x - ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 15), .1f, (Destination1.z - (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 3)));

						Destination5 = new Vector3 ((Destination1.x - ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 17), .1f, (Destination1.z - (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 5)));

						Destination6 = new Vector3 ((Destination1.x - ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 18), .1f, (Destination1.z - (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 7)));

						Destination7 = new Vector3 ((Destination1.x - ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 19), .1f, (Destination1.z - (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 9)));
					} else if (!xAdd && zAdd) {

						Destination2 = new Vector3 ((Destination1.x - ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 10), .1f, (Destination1.z + (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 1)));

						Destination3 = new Vector3 ((Destination1.x - ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 12), .1f, (Destination1.z + (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 2)));

						Destination4 = new Vector3 ((Destination1.x - ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 15), .1f, (Destination1.z + (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 3)));

						Destination5 = new Vector3 ((Destination1.x - ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 17), .1f, (Destination1.z + (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 5)));

						Destination6 = new Vector3 ((Destination1.x - ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 18), .1f, (Destination1.z + (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 7)));

						Destination7 = new Vector3 ((Destination1.x - ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 19), .1f, (Destination1.z + (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 9)));
					}
				}
			} 
			// IF THE CAR IS GOING STRAIGHT
			else {
				if (vertical) {

					if (xAdd && zAdd) {

						Destination2 = new Vector3 (Destination1.x + (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 3), .1f, (Destination1.z + ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 3));

						Destination3 = new Vector3 (Destination1.x + (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 6), .1f, (Destination1.z + ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 6));

						Destination4 = new Vector3 (Destination1.x + (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 9), .1f, (Destination1.z + ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 9));

						Destination5 = new Vector3 (Destination1.x + (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 12), .1f, (Destination1.z + ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 12));

						Destination6 = new Vector3 (Destination1.x + (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 15), .1f, (Destination1.z + ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 15));

						Destination7 = new Vector3 (Destination1.x + (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 18), .1f, (Destination1.z + ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 18));

					} else if (xAdd && !zAdd) {

						Destination2 = new Vector3 (Destination1.x + (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 3), .1f, (Destination1.z - ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 3));

						Destination3 = new Vector3 (Destination1.x + (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 6), .1f, (Destination1.z - ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 6));

						Destination4 = new Vector3 (Destination1.x + (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 9), .1f, (Destination1.z - ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 9));

						Destination5 = new Vector3 (Destination1.x + (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 12), .1f, (Destination1.z - ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 12));

						Destination6 = new Vector3 (Destination1.x + (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 15), .1f, (Destination1.z - ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 15));

						Destination7 = new Vector3 (Destination1.x + (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 18), .1f, (Destination1.z - ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 18));

					} else if (!xAdd && !zAdd) {

						Destination2 = new Vector3 (Destination1.x - (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 3), .1f, (Destination1.z - ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 3));

						Destination3 = new Vector3 (Destination1.x - (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 6), .1f, (Destination1.z - ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 6));

						Destination4 = new Vector3 (Destination1.x - (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 9), .1f, (Destination1.z - ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 9));

						Destination5 = new Vector3 (Destination1.x - (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 12), .1f, (Destination1.z - ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 12));

						Destination6 = new Vector3 (Destination1.x - (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 15), .1f, (Destination1.z - ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 15));

						Destination7 = new Vector3 (Destination1.x - (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 18), .1f, (Destination1.z - ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 18));

					} else if (!xAdd && zAdd) {

						Destination2 = new Vector3 (Destination1.x - (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 3), .1f, (Destination1.z + ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 3));

						Destination3 = new Vector3 (Destination1.x - (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 6), .1f, (Destination1.z + ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 6));

						Destination4 = new Vector3 (Destination1.x - (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 9), .1f, (Destination1.z + ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 9));

						Destination5 = new Vector3 (Destination1.x - (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 12), .1f, (Destination1.z + ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 12));

						Destination6 = new Vector3 (Destination1.x - (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 15), .1f, (Destination1.z + ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 15));

						Destination7 = new Vector3 (Destination1.x - (((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 18), .1f, (Destination1.z + ((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 18));
					}
				} 
				//-------------------------------------------------------------------------------------------------------------------------------------------------------------
				else {

					if (xAdd && zAdd) {

						Destination2 = new Vector3 ((Destination1.x + ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 3), .1f, (Destination1.z + (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 3)));

						Destination3 = new Vector3 ((Destination1.x + ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 6), .1f, (Destination1.z + (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 6)));

						Destination4 = new Vector3 ((Destination1.x + ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 9), .1f, (Destination1.z + (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 9)));

						Destination5 = new Vector3 ((Destination1.x + ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 12), .1f, (Destination1.z + (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 12)));

						Destination6 = new Vector3 ((Destination1.x + ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 15), .1f, (Destination1.z + (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 15)));

						Destination7 = new Vector3 ((Destination1.x + ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 18), .1f, (Destination1.z + (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 18)));

					} else if (xAdd && !zAdd) {

						Destination2 = new Vector3 ((Destination1.x + ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 3), .1f, (Destination1.z - (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 3)));

						Destination3 = new Vector3 ((Destination1.x + ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 6), .1f, (Destination1.z - (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 6)));

						Destination4 = new Vector3 ((Destination1.x + ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 9), .1f, (Destination1.z - (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 9)));

						Destination5 = new Vector3 ((Destination1.x + ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 12), .1f, (Destination1.z - (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 12)));

						Destination6 = new Vector3 ((Destination1.x + ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 15), .1f, (Destination1.z - (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 15)));

						Destination7 = new Vector3 ((Destination1.x + ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 18), .1f, (Destination1.z - (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 18)));
					} else if (!xAdd && !zAdd) {

						Destination2 = new Vector3 ((Destination1.x - ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 3), .1f, (Destination1.z - (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 3)));

						Destination3 = new Vector3 ((Destination1.x - ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 6), .1f, (Destination1.z - (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 6)));

						Destination4 = new Vector3 ((Destination1.x - ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 9), .1f, (Destination1.z - (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 9)));

						Destination5 = new Vector3 ((Destination1.x - ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 12), .1f, (Destination1.z - (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 12)));

						Destination6 = new Vector3 ((Destination1.x - ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 15), .1f, (Destination1.z - (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 15)));

						Destination7 = new Vector3 ((Destination1.x - ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 18), .1f, (Destination1.z - (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 18)));
					} else if (!xAdd && zAdd) {

						Destination2 = new Vector3 ((Destination1.x - ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 3), .1f, (Destination1.z + (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 3)));

						Destination3 = new Vector3 ((Destination1.x - ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 6), .1f, (Destination1.z + (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 6)));

						Destination4 = new Vector3 ((Destination1.x - ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 9), .1f, (Destination1.z + (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 9)));

						Destination5 = new Vector3 ((Destination1.x - ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 12), .1f, (Destination1.z + (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 12)));

						Destination6 = new Vector3 ((Destination1.x - ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 15), .1f, (Destination1.z + (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 15)));

						Destination7 = new Vector3 ((Destination1.x - ((Mathf.Abs (Destination1.x - Destination8.x)) / 20) * 18), .1f, (Destination1.z + (((Mathf.Abs (Destination1.z - Destination8.z)) / 20) * 18)));
					}
				}
			
			
			}

			Destinations.Add (Destination0);
			Destinations.Add (Destination1);
			Destinations.Add (Destination2);
			Destinations.Add (Destination3);
			Destinations.Add (Destination4);
			Destinations.Add (Destination5);
			Destinations.Add (Destination6);
			Destinations.Add (Destination7);
			Destinations.Add (Destination8);
			Destinations.Add (Destination9);

			currentDestination = Destinations [StartingDestination];

		}
	}

	void debugLines(){
		Debug.DrawLine (Destination0, Destination1, Color.green);
		Debug.DrawLine (Destination1, Destination2, Color.green);
		Debug.DrawLine (Destination2, Destination3, Color.green);
		Debug.DrawLine (Destination3, Destination4, Color.green);
		Debug.DrawLine (Destination4, Destination5, Color.green);
		Debug.DrawLine (Destination5, Destination6, Color.green);
		Debug.DrawLine (Destination6, Destination7, Color.green);
		Debug.DrawLine (Destination7, Destination8, Color.green);
		Debug.DrawLine (Destination8, Destination9, Color.green);
	}

	public void decideSpawnDirection(){
		
		float y = transform.rotation.y;
		if (y > -0.5 && y < 0.5) { //S
			carDirectionFrom = 2;
		} else if (y > 0.9 && y < 1.5) { //N
			carDirectionFrom = 0;
		} else if (y > 0.5 && y < 0.9) {//W
			carDirectionFrom = 3;
		} else {
			carDirectionFrom = 1;
		}
		if (carDirectionFrom == 1 || carDirectionFrom == 3) {
			vertical = false;
		} else {
			vertical = true;
		}
	}
}
