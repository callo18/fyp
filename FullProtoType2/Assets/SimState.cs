﻿using UnityEngine;
using System.Collections;

public class SimState : MonoBehaviour {

	enum State {paused, setup, running, finished};
	public GameObject world;
	private WorldScript ws; 

	// Use this for initialization
	void Start () {
		ws = world.GetComponent<WorldScript>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void displayJunctionTemplate(){
		ws.CreateJunction (1, 2, 3, 4, 5, 6, 7, 4);
	}
}
