﻿using UnityEngine;
using System.Collections;

public class DestinationPoint : MonoBehaviour {

	public GameObject Collector;


	void Update () {
		drawDebugLine ();
	}

	public void drawDebugLine(){
		if (Collector != null) {
			Debug.DrawLine (transform.position, Collector.transform.position, Color.red);
		}
	}
}
