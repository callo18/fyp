﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class DecisionPointController : MonoBehaviour {

	private GameObject junctionPiece;
	public List<GameObject> NDecisionPointers;
	public List<GameObject> EDecisionPointers;
	public List<GameObject> SDecisionPointers;
	public List<GameObject> WDecisionPointers;

	public List<GameObject> NDestinationPointers;
	public List<GameObject> EDestinationPointers;
	public List<GameObject> SDestinationPointers;
	public List<GameObject> WDestinationPointers;

	//public bool NturnL,NturnR,NgoS, EturnL,EturnR,EgoS, SturnL,SturnR,SgoS,WturnL,WturnR,WgoS;

	public GameObject settingsUI;


	/*-- Called by the GAMECONTROLLER after building the world.
	 * assignDecisionPointerAndDestinationsToList ();
	 * // Assign all the objects needed for the Path deciding eg Decision Points and Destination Points.

	 * assignAllPossibleDestinationsToDecisionPoints ();
	 * // Gives each Decsion Point the Possible Destination Points that it can travel to

	 * assignDestinationPointsTheirCollectors ();
	 * Link the Destination points to their Collectors

	 * assignDecisionPointsTheirSpawners ();
	 * // Link the Decision Points to their Spawners
	*/

	public void SetUpDecisionPointers() {
		
		assignDecisionPointerAndDestinationsToList ();
		assignAllPossibleDestinationsToDecisionPoints ();
		assignDestinationPointsTheirCollectors ();
		assignDecisionPointsTheirSpawners ();
	}

	//--
	public void assignDecisionPointerAndDestinationsToList(){

		NDecisionPointers = junctionPiece.GetComponent<JunctionPiece> ().NDecisionPointers;
		EDecisionPointers = junctionPiece.GetComponent<JunctionPiece> ().EDecisionPointers;
		SDecisionPointers = junctionPiece.GetComponent<JunctionPiece> ().SDecisionPointers;
		WDecisionPointers = junctionPiece.GetComponent<JunctionPiece> ().WDecisionPointers;

		NDestinationPointers = junctionPiece.GetComponent<JunctionPiece> ().NDestinationPointers;
		EDestinationPointers = junctionPiece.GetComponent<JunctionPiece> ().EDestinationPointers;
		SDestinationPointers = junctionPiece.GetComponent<JunctionPiece> ().SDestinationPointers;
		WDestinationPointers = junctionPiece.GetComponent<JunctionPiece> ().WDestinationPointers;
	}
		
	//--
	public void assignAllPossibleDestinationsToDecisionPoints(){

		foreach(GameObject dp in NDecisionPointers){

			dp.GetComponent<DecisionPoint> ().StraightDestinationPointers = SDestinationPointers;
			dp.GetComponent<DecisionPoint> ().RightDestinationPointers = WDestinationPointers;
			dp.GetComponent<DecisionPoint> ().LeftDestinationPointers = EDestinationPointers;
		}

		foreach(GameObject dp in EDecisionPointers){

			dp.GetComponent<DecisionPoint> ().StraightDestinationPointers = WDestinationPointers;
			dp.GetComponent<DecisionPoint> ().RightDestinationPointers = NDestinationPointers;
			dp.GetComponent<DecisionPoint> ().LeftDestinationPointers = SDestinationPointers;
		}

		foreach(GameObject dp in SDecisionPointers){

			dp.GetComponent<DecisionPoint> ().StraightDestinationPointers = NDestinationPointers;
			dp.GetComponent<DecisionPoint> ().RightDestinationPointers = EDestinationPointers;
			dp.GetComponent<DecisionPoint> ().LeftDestinationPointers = WDestinationPointers;
		}

		foreach(GameObject dp in WDecisionPointers){

			dp.GetComponent<DecisionPoint> ().StraightDestinationPointers = EDestinationPointers;
			dp.GetComponent<DecisionPoint> ().RightDestinationPointers = SDestinationPointers;
			dp.GetComponent<DecisionPoint> ().LeftDestinationPointers = NDestinationPointers;
		}
	}
		
	//--
	public void assignDestinationPointsTheirCollectors(){

		foreach(GameObject dp in NDestinationPointers){

			dp.GetComponent<DestinationPoint> ().Collector = dp.transform.parent.Find ("VehicleCollector").gameObject;
		}

		foreach(GameObject dp in EDestinationPointers){

			dp.GetComponent<DestinationPoint> ().Collector = dp.transform.parent.Find ("VehicleCollector").gameObject;
		}

		foreach(GameObject dp in SDestinationPointers){

			dp.GetComponent<DestinationPoint> ().Collector = dp.transform.parent.Find ("VehicleCollector").gameObject;
		}

		foreach(GameObject dp in WDestinationPointers){

			dp.GetComponent<DestinationPoint> ().Collector = dp.transform.parent.Find ("VehicleCollector").gameObject;
		}

	}

	//--
	public void assignDecisionPointsTheirSpawners(){

		foreach(GameObject dp in NDecisionPointers){

			dp.GetComponent<DecisionPoint> ().Spawner = dp.transform.parent.Find ("VehicleSpawner").gameObject;
		}

		foreach(GameObject dp in EDecisionPointers){

			dp.GetComponent<DecisionPoint> ().Spawner = dp.transform.parent.Find ("VehicleSpawner").gameObject;
		}

		foreach(GameObject dp in SDecisionPointers){

			dp.GetComponent<DecisionPoint> ().Spawner = dp.transform.parent.Find ("VehicleSpawner").gameObject;
		}

		foreach(GameObject dp in WDecisionPointers){

			dp.GetComponent<DecisionPoint> ().Spawner = dp.transform.parent.Find ("VehicleSpawner").gameObject;
		}
	}
	/* Called by FINISHED BUTTON in the UI
	 * 
	 * AssignEachDecisionPointItsChosenPossibleDirections ();
	 * // After finalising the choosing lane direction phase, each decision creates a list of its available directions
	 * 
	 * hideDecisionPoints ();
	 * // Hides the Elements of the decision points that will not be in the running simulation
	*/


	public void endDecisionPhase(){

		AssignEachDecisionPointItsChosenPossibleDirections ();
		hideDecisionPoints ();
	}

	//--
	public void AssignEachDecisionPointItsChosenPossibleDirections(){

		foreach(GameObject dp in NDecisionPointers){

			dp.GetComponent<DecisionPoint> ().AssignDecisionPointItsChosenPossibleDirections ();
		}

		foreach(GameObject dp in EDecisionPointers){

			dp.GetComponent<DecisionPoint> ().AssignDecisionPointItsChosenPossibleDirections ();
		}

		foreach(GameObject dp in SDecisionPointers){

			dp.GetComponent<DecisionPoint> ().AssignDecisionPointItsChosenPossibleDirections ();
		}

		foreach(GameObject dp in WDecisionPointers){

			dp.GetComponent<DecisionPoint> ().AssignDecisionPointItsChosenPossibleDirections ();
		}

	}


	//--
	public void hideDecisionPoints(){
		
		foreach (GameObject dp in NDecisionPointers) {

			dp.GetComponent<Renderer> ().enabled = false;
			dp.transform.FindChild ("Straight").gameObject.SetActive (false);
			dp.transform.FindChild ("Left").gameObject.SetActive (false);
			dp.transform.FindChild ("Right").gameObject.SetActive (false);
		}

		foreach (GameObject dp in EDecisionPointers) {

			dp.GetComponent<Renderer> ().enabled = false;
			dp.transform.FindChild ("Straight").gameObject.SetActive (false);
			dp.transform.FindChild ("Left").gameObject.SetActive (false);
			dp.transform.FindChild ("Right").gameObject.SetActive (false);
		}

		foreach (GameObject dp in SDecisionPointers) {

			dp.GetComponent<Renderer> ().enabled = false;
			dp.transform.FindChild ("Straight").gameObject.SetActive (false);
			dp.transform.FindChild ("Left").gameObject.SetActive (false);
			dp.transform.FindChild ("Right").gameObject.SetActive (false);
		}

		foreach (GameObject dp in WDecisionPointers) {

			dp.GetComponent<Renderer> ().enabled = false;
			dp.transform.FindChild ("Straight").gameObject.SetActive (false);
			dp.transform.FindChild ("Left").gameObject.SetActive (false);
			dp.transform.FindChild ("Right").gameObject.SetActive (false);
		}
	}

	/*
	 * 
	 * 
	 *  EXTRA
	 * 
	 * 
	 * 
	*/

	public void reopenDecisionPhase(){
		
		foreach (GameObject dp in NDecisionPointers) {

			dp.GetComponent<Renderer> ().enabled = true;
			dp.transform.FindChild ("Straight").gameObject.SetActive (true);
			dp.transform.FindChild ("Left").gameObject.SetActive (true);
			dp.transform.FindChild ("Right").gameObject.SetActive (true);
		}

		foreach (GameObject dp in EDecisionPointers) {

			dp.GetComponent<Renderer> ().enabled = true;
			dp.transform.FindChild ("Straight").gameObject.SetActive (true);
			dp.transform.FindChild ("Left").gameObject.SetActive (true);
			dp.transform.FindChild ("Right").gameObject.SetActive (true);
		}

		foreach (GameObject dp in SDecisionPointers) {

			dp.GetComponent<Renderer> ().enabled = true;
			dp.transform.FindChild ("Straight").gameObject.SetActive (true);
			dp.transform.FindChild ("Left").gameObject.SetActive (true);
			dp.transform.FindChild ("Right").gameObject.SetActive (true);
		}

		foreach (GameObject dp in WDecisionPointers) {

			dp.GetComponent<Renderer> ().enabled = true;
			dp.transform.FindChild ("Straight").gameObject.SetActive (true);
			dp.transform.FindChild ("Left").gameObject.SetActive (true);
			dp.transform.FindChild ("Right").gameObject.SetActive (true);
		}
	}

	//	public void getSettings(){
	//		NturnL = settingsUI.transform.Find ("N").Find ("E").GetComponent<Toggle> ().isOn;
	//		NturnR = settingsUI.transform.Find ("N").Find ("W").GetComponent<Toggle> ().isOn;
	//		NgoS = settingsUI.transform.Find ("N").Find ("S").GetComponent<Toggle> ().isOn;
	//
	//		SturnL = settingsUI.transform.Find ("S").Find ("W").GetComponent<Toggle> ().isOn;
	//		SturnR = settingsUI.transform.Find ("S").Find ("E").GetComponent<Toggle> ().isOn;
	//		SgoS = settingsUI.transform.Find ("S").Find ("N").GetComponent<Toggle> ().isOn;
	//
	//		EturnL = settingsUI.transform.Find ("E").Find ("S").GetComponent<Toggle> ().isOn;
	//		EturnR = settingsUI.transform.Find ("E").Find ("N").GetComponent<Toggle> ().isOn;
	//		EgoS = settingsUI.transform.Find ("E").Find ("W").GetComponent<Toggle> ().isOn;
	//
	//		WturnL = settingsUI.transform.Find ("W").Find ("N").GetComponent<Toggle> ().isOn;
	//		WturnR = settingsUI.transform.Find ("W").Find ("S").GetComponent<Toggle> ().isOn;
	//		WgoS = settingsUI.transform.Find ("W").Find ("E").GetComponent<Toggle> ().isOn;
	//
	//	}
}
