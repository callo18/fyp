﻿using UnityEngine;
using System.Collections;

public class GameManagerScript : MonoBehaviour {

	private GUIManagerScript guiMan;
	private WorldObjectsManagerScript worldObjectsMan;
	private WorldBuildingManager worldBuildingMan;
	private EditingLanesManager editingLanesMan;
	private PreGameSettingManager gameSettingsMan;
	private SimulationManager simMan;
	private PostGameReportManager postGameReportMan;
	private CameraController cameraman;
	private SimulationStats simStat;
	// GAMESTATE ENUM FOR ALL THE POSSIBLE GAME STATES
	public enum GameState {IntroScreen, BuildingPhase, EditingPhase, GameSettingsPhase, SimulationPhase, PostGameReportPhase};

	public GameState currentPhase;

	void Start () {

		// INITIALIZING GAME MANAGAGERS
		guiMan = GameObject.Find ("GUIManager").GetComponent<GUIManagerScript> ();

		worldObjectsMan = GameObject.Find ("WorldObjectsManager").GetComponent<WorldObjectsManagerScript> ();

		worldBuildingMan = GameObject.Find ("WorldBuildingManager").GetComponent<WorldBuildingManager> ();

		editingLanesMan = GameObject.Find ("EditingLanesManager").GetComponent<EditingLanesManager> ();

		gameSettingsMan = GameObject.Find ("GameSettingsManager").GetComponent<PreGameSettingManager> ();

		simMan = GameObject.Find ("SimulationManager").GetComponent<SimulationManager> ();

		simStat = GameObject.Find ("SimulationManager").GetComponent<SimulationStats> ();


		postGameReportMan = GameObject.Find ("PostGameReportManager").GetComponent<PostGameReportManager> ();

		cameraman = GameObject.Find ("CameraLocationManager").GetComponent<CameraController> ();

		// SETTING UP START GAME
		currentPhase = GameState.IntroScreen;
		guiMan.IntroScreen ();
	}


	//USED TO CHANGE TO STATE OF THE GAME * Used for buttons (public)
	public void ChangeToIntroScreen(){
		PrepareForNewGame();

		currentPhase = GameState.IntroScreen;
		guiMan.IntroScreen ();
	}

	public void ChangeToWorldBuilderScreen(){
		PrepareForNewGame();

		currentPhase = GameState.BuildingPhase;
		guiMan.JunctionBuildingScreen ();
	}

	public void ChangeToEditingLanesScreen(){

		PrepForEditingLanes ();

		currentPhase = GameState.EditingPhase;
		guiMan.EditingLanesScreen ();
	}

	public void ChangePreGameSettingsScreen(){

		FinishEditingLanes ();

		currentPhase = GameState.GameSettingsPhase;
		guiMan.GameSettingScreen ();
	}

	public void ChangeToSimulationScreen(){

		FinishEditingLanes ();

		currentPhase = GameState.SimulationPhase;
		guiMan.SimulationScreen ();
	}

	public void ChangeToPostGameReportScreen(){

		currentPhase = GameState.PostGameReportPhase;
		guiMan.PostGameReportScreen ();
	}
	//---------------- SwitchingPhase Functionality ------------------//

	public void SetUpNewGameAfterBuild(){
		worldObjectsMan.setUpGame ();
		cameraman.getCameras ();

	}

	public void PrepareForNewGame(){
		worldObjectsMan.prepareForNewGame();
		cameraman.editorMode = false;
		cameraman.simMode = false;
	}

	public void PrepForEditingLanes(){
		editingLanesMan.showDecisionPoints ();
		cameraman.editorMode = true;
		cameraman.simMode = false;
	}

	public void FinishEditingLanes(){
		editingLanesMan.finish ();
		cameraman.editorMode = false;
	}

	public void ApplySettings(){
		gameSettingsMan.ApplySettings ();
		cameraman.simMode = true;
	}

	public void ApplyEditlanesSettings(){
		gameSettingsMan.ApplyEditLanesSettings ();
	}
//	public void ChangeBackToEditingLanesScreen(){
//		currentPhase = GameState.EditingPhase;
//		guiMan.EditingLanesScreen ();
//
//		editingLanesMan.start();
//	}
//
//	public void ChangeBackToPreGameSettingsScreen(){
//		currentPhase = GameState.GameSettingsPhase;
//	}
}
