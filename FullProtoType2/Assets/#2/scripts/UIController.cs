﻿using UnityEngine;
using System.Collections;

public class UIController : MonoBehaviour {

	//For the Junction Builder
	public GameObject jbc; // junction builder canvas
	public GameObject pgo;
	public GameObject igo;
	public GameObject lds;

	public GameObject GameController;



	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void BuildingState(){
		jbc.active = true;
		pgo.active = false;
		igo.active = false;
		lds.active = false;
	}

	public void LaneDecisionState(){
		jbc.active = false;
		pgo.active = false;
		igo.active = false;
		lds.active = true;
	}

	public void PreGameState(){
		jbc.active = false;
		pgo.active = true;
		igo.active = false;
		lds.active = false;
	}

	public void GameState(){
		jbc.active = false;
		pgo.active = false;
		igo.active = true;
		lds.active = false;
	}

	public void ToggleJunctionBuilder(){
		if (jbc.active) {
			jbc.active = false;
		} else {
			jbc.active = true;
		}
	}

	public void TogglePreGameOptions(){
		if (pgo.active) {
			pgo.active = false;
		} else {
			pgo.active = true;
		}
	}

	public void ToggleInGameOptions(){
		igo.active = true;
	}
}
