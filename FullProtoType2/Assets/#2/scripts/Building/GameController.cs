﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

	public bool running;

	public GameObject vehicleSpawnerController;
	public GameObject decisionPointerController;


	public void SetUpGame () {
		//
		vehicleSpawnerController.GetComponent<VehicleSpawnerController> ().SetUpSpawners();
		//
		decisionPointerController.GetComponent<DecisionPointController> ().SetUpDecisionPointers();
		//TODO: for collectors
		//vehicleSpawnerController.GetComponent<VehicleSpawnerController> ().SetUpSpawners();
		//TODO: for trafficLights
		//vehicleSpawnerController.GetComponent<VehicleSpawnerController> ().SetUpSpawners();
	}








	public void SaveSettings () {
		print ("Settings Saved and Applied");
		vehicleSpawnerController.GetComponent<VehicleSpawnerController> ().applySpawnerSettings();
	}

	public void TurnOnSimulation(){
		if(!running){
			vehicleSpawnerController.GetComponent<VehicleSpawnerController> ().TurnOnAllSpawners();
			running = true;
			print ("Started");
		}
	}

	public void TurnOffSimulation(){
		if (running) {
			vehicleSpawnerController.GetComponent<VehicleSpawnerController> ().TurnOffAllSpawners ();
			running = false;
			print ("Finished");
		}
	}
		
}
