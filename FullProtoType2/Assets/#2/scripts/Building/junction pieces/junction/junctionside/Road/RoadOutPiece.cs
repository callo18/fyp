﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RoadOutPiece : MonoBehaviour {

	private float lanesOut;

	private float sideWidth;
	private float roadLength = 40;

	private Transform objectModel;

	public GameObject laneOutPrefab,trafficLightsPrefab,theTrafficLights;

	private List<GameObject> myLanes = new List<GameObject> ();
	private List<GameObject> myCollectors = new List<GameObject> ();
	private List<GameObject> myDestinationPoints = new List<GameObject> ();

	public void setUpRoad(float li,float size){

		sideWidth = size;
		lanesOut = li;

		calculateVariables ();

		resizePiece (lanesOut);

		setLocationOfPiece ();

		placeLights (lanesOut);

		setUpLanes ();
	}

	void calculateVariables(){

		objectModel = this.transform.FindChild ("RoadOutModel");

		laneOutPrefab = GameObject.Find ("LaneOut");

		trafficLightsPrefab = GameObject.Find ("TrafficLight");

	}

	void resizePiece (float li){

		objectModel.localScale = new Vector3 (lanesOut, 0.25f, roadLength);

	}

	void setUpLanes(){

		float positionFromLeft = 0;
		positionFromLeft = (-lanesOut / 2)+.5f;

		for (int i = 0; i < lanesOut; i++) {
			GameObject laneOut = (GameObject)Instantiate (laneOutPrefab, new Vector3 (0, 0, 0), transform.localRotation);

			LaneOutPiece lop = laneOut.GetComponent<LaneOutPiece> ();

			laneOut.transform.parent = this.transform.FindChild ("Lanes");

			lop.name = "Lane Out";
			laneOut.transform.localRotation = Quaternion.identity;
			laneOut.transform.localPosition = new Vector3(positionFromLeft,0,0);
			positionFromLeft += 1;
			myLanes.Add (laneOut);
			// Collector Control
			GameObject Collector;
			Collector = laneOut.transform.FindChild ("VehicleCollector").gameObject;
			myCollectors.Add (Collector);
			// Destination Point control
			GameObject DestinationPoint;
			DestinationPoint = laneOut.transform.FindChild ("DestinationPoint").gameObject;
			myDestinationPoints.Add (DestinationPoint);

		}

	}

	void placeLights(float lo){

		GameObject TrafficLights = (GameObject)Instantiate(trafficLightsPrefab,new Vector3 (0, 0, 0), transform.localRotation);

		TrafficLights tl = TrafficLights.GetComponent<TrafficLights> ();
		//Sets up new object.
		TrafficLights.transform.parent = this.transform.FindChild ("TrafficLights");
		tl.name = "TrafficLights";
		//tl.setUpLights (lo);

		TrafficLights.transform.Find("TrafficLightsModel").gameObject.SetActive(false);

		theTrafficLights = TrafficLights;
	}

	void setLocationOfPiece(){

		float roadCenterPoint = -(sideWidth/2) + (lanesOut/2);
		transform.localPosition = new Vector3(roadCenterPoint,0,0);

	}

	//SORTING

	public GameObject getTrafficLights(){
		return theTrafficLights;
	}

	public List<GameObject> getCollectors(){

		return myCollectors;

	}

	public List<GameObject> getDestinationPoints(){

		return myDestinationPoints;

	}
}
