﻿using UnityEngine;
using System.Collections;

public class VehicleSpawner : MonoBehaviour {

	public GameObject [] vehicles;
	private float minSpawnTime = 0.2f;
	private float maxSpawnTime = 2.0f;
	// Use this for initialization
	void Start()
	{
		StartCoroutine(SpawnVehicle());
	}

	// Update is called once per frame
	void Update () {

	}

	//Spawns a vehicle every x seconds depending on min and max numbers set.
	IEnumerator SpawnVehicle()
	{
		while(true)
		{
			GameObject cube = (GameObject)Instantiate(vehicles[0], this.transform.position, this.transform.rotation);
			yield return new WaitForSeconds(Random.Range(minSpawnTime, maxSpawnTime));
		}
	}


}
