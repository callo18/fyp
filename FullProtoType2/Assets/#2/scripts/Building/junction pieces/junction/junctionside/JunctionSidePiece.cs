﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class JunctionSidePiece : MonoBehaviour {

	private float lanesIn,lanesOut;
	private float sideWidth;

	private float sideLength = 40;
	public float pathWidth = 0.3f;
	private bool centerIsland = false;
	private float sizeOfIsland = 0.4f;

	private Transform objectModel;

	public GameObject roadInPrefab, roadOutPrefab;

	public GameObject pathPrefab,pathLanePrefab,mainpathPrefab;
	//storing
	private GameObject theRoadIn;
	private GameObject theRoadOut;
	//organisation
	private GameObject myTrafficLights;
	private List<GameObject> mySpawners = new List<GameObject> ();
	private List<GameObject> myCollectors = new List<GameObject> ();
	private List<GameObject> myDecisionPoints = new List<GameObject> ();
	private List<GameObject> myDestinationPoints = new List<GameObject> ();
	
	// Update is called once per frame
	void Update () {
	
	}

	public void setUpSide(float li,float lo, float w){

		lanesIn = li;
		lanesOut = lo;
		sideWidth = w;

		calculateVariables ();

		resizePiece (lanesIn,lanesOut);

		buildThePaths ();

		if (centerIsland) {
			placeIsland ();
		}

		placeRoads (lanesIn,lanesOut);

	}

	void calculateVariables(){

		if (lanesIn > 0 && lanesOut > 0) {
			centerIsland = true;
		} else {
			centerIsland = false;
		}

		int randomPath = Random.Range(1,7);

		objectModel = this.transform.FindChild ("JunctionSideModel");

		roadInPrefab = GameObject.Find ("RoadIn");
		roadOutPrefab = GameObject.Find ("RoadOut");
		pathPrefab = GameObject.Find ("Path");
		pathLanePrefab = GameObject.Find("PathLane");
		mainpathPrefab = GameObject.Find ("MainPath"+randomPath);
	}

	void resizePiece (float li, float lo){

		objectModel.localScale = new Vector3 (sideWidth, 0.1f, sideLength);

	}

	void placeRoads(float li,float lo){

		if(li>0){

			GameObject roadIn = (GameObject)Instantiate(roadInPrefab,new Vector3 (0, 0, 0), transform.localRotation);

			RoadInPiece rip = roadIn.GetComponent<RoadInPiece> ();

			//Sets up new object.
			roadIn.transform.parent = this.transform.FindChild ("Roads");
			rip.name = "RoadIn";
			rip.setUpRoad (li, sideWidth);
			theRoadIn = roadIn;

		}

		if(lo>0){

			GameObject roadOut = (GameObject)Instantiate(roadOutPrefab,new Vector3 (0, 0, 0), transform.localRotation);

			RoadOutPiece rop = roadOut.GetComponent<RoadOutPiece> ();

			//Sets up new object.
			roadOut.transform.parent = this.transform.FindChild ("Roads");
			rop.name = "RoadOut";
			rop.setUpRoad (lo, sideWidth);
			theRoadOut = roadOut;

		}
	}

	//Messy method.. Cant be improved. Builds the paths for each side.
	void buildThePaths(){
		// build the 1st path.
		float x =  ((sideWidth / 2) + pathWidth / 2);
		float z =  (sideLength / 2);

		GameObject path = (GameObject)Instantiate(mainpathPrefab,new Vector3 (0, 0, 0), transform.localRotation);

		path.transform.parent = this.transform.FindChild("Paths");
		path.transform.Rotate(new Vector3(0,90,0));
		path.transform.localPosition = new Vector3(x,0,z);
		path.transform.localScale = new Vector3 (1, 1, 1);

		//build the 2nd path.
		x =  -((sideWidth / 2) + pathWidth / 2);
		z =  (sideLength / 2);

		path = (GameObject)Instantiate(mainpathPrefab,new Vector3 (0, 0, 0), transform.localRotation);

		path.transform.parent = this.transform.FindChild("Paths");
		path.transform.Rotate(new Vector3(0,270,0));
		path.transform.localPosition = new Vector3(x,0,z);
		path.transform.localScale = new Vector3 (1, 1, 1);

	}

	void placeIsland(){

		GameObject island = (GameObject)Instantiate(pathPrefab,new Vector3 (0, 0, 0), transform.localRotation);

		island.transform.parent = this.transform.FindChild("Paths");
		island.name = "island";
		island.transform.localPosition = new Vector3(-(sideWidth/2) + lanesOut + (sizeOfIsland/2),0,sizeOfIsland);
		island.transform.localScale = new Vector3 (sizeOfIsland, .2f, sizeOfIsland*2);

		GameObject islandLane = (GameObject)Instantiate(pathLanePrefab,new Vector3 (0, 0, 0), transform.localRotation);

		islandLane.transform.parent = this.transform.FindChild("Paths");
		islandLane.name = "islandLane";
		islandLane.transform.localPosition = new Vector3(-(sideWidth/2) + lanesOut + (sizeOfIsland/2),0,sideLength/2);
		islandLane.transform.localScale = new Vector3 (sizeOfIsland, .1f, sideLength);

	}

	//SORTING

	public GameObject  getMyTrafficLights(){
		// 1st is road in trafficlights
		if (lanesIn > 0) {
			myTrafficLights = (theRoadIn.GetComponent<RoadInPiece> ().getTrafficLights ());
		}
		return myTrafficLights;


	}

	public List<GameObject>  getMySpawners(){

		if(lanesIn > 0){
			mySpawners = theRoadIn.GetComponent<RoadInPiece> ().getSpawners ();
		}

		return mySpawners;

	}

	public List<GameObject>  getMyCollectors(){
		if(lanesOut > 0){
			myCollectors = theRoadOut.GetComponent<RoadOutPiece> ().getCollectors ();
		}
		return myCollectors;
	}

	public List<GameObject>  getMyDecisionPoints(){
		if(lanesIn > 0){
			myDecisionPoints = theRoadIn.GetComponent<RoadInPiece> ().getDecisionPoints ();
		}
		return myDecisionPoints;
	}

	public List<GameObject>  getMyDestinationPoints(){
		if(lanesOut > 0){
			myDestinationPoints = theRoadOut.GetComponent<RoadOutPiece> ().getDestinationPoints ();
		}
		return myDestinationPoints;
	}

}
