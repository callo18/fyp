﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RoadInPiece : MonoBehaviour {

	private float lanesIn;

	private float sideWidth;
	private float roadLength = 40;

	private Transform objectModel;

	public GameObject laneInPrefab,trafficLightsPrefab,theTrafficLights;

	//SORTING
	private List<GameObject> myLanes = new List<GameObject> ();
	private List<GameObject> mySpawners = new List<GameObject> ();
	public List<GameObject> myDecisionPoints = new List<GameObject> ();

	public void setUpRoad(float li,float size){

		sideWidth = size;
		lanesIn = li;

		calculateVariables ();

		resizePiece (lanesIn);

		setLocationOfPiece ();

		placeLights (lanesIn);

		setUpLanes ();
	}

	void calculateVariables(){

		objectModel = this.transform.FindChild ("RoadInModel");

		laneInPrefab = GameObject.Find ("LaneIn");

		trafficLightsPrefab = GameObject.Find ("TrafficLight");

	}

	void resizePiece (float li){

		objectModel.localScale = new Vector3 (lanesIn, .25f, roadLength);

	}

	void setUpLanes(){
		float positionFromLeft = 0;
		positionFromLeft = (-lanesIn / 2)+.5f;

		for (int i = 0; i < lanesIn; i++) {
			GameObject laneIn = (GameObject)Instantiate (laneInPrefab, new Vector3 (0, 0, 0), transform.localRotation);

			LaneInPiece lip = laneIn.GetComponent<LaneInPiece> ();

			laneIn.transform.parent = this.transform.FindChild ("Lanes");

			lip.name = "Lane In";
			laneIn.transform.localRotation = Quaternion.identity;
			laneIn.transform.localPosition = new Vector3(positionFromLeft,0,0);
			positionFromLeft += 1;
			myLanes.Add (laneIn);
			// Spawner Control
			GameObject Spawner;
			Spawner = laneIn.transform.FindChild ("VehicleSpawner").gameObject;
			mySpawners.Add (Spawner);
			// DecisionPoints Control
			GameObject DecisionPoint;
			DecisionPoint = laneIn.transform.FindChild ("DecisionPoint").gameObject;
			myDecisionPoints.Add (DecisionPoint);

		}
	}
		
	void placeLights(float li){
		GameObject TrafficLights = (GameObject)Instantiate(trafficLightsPrefab,new Vector3 (0, 0, 0), transform.localRotation);

		TrafficLights tl = TrafficLights.GetComponent<TrafficLights> ();

		//Sets up new object.
		TrafficLights.transform.parent = this.transform.FindChild ("TrafficLights");
		tl.name = "TrafficLights";
		tl.setUpLights (li);

		theTrafficLights = TrafficLights;
	}

	void setLocationOfPiece(){

		float roadCenterPoint = (sideWidth/2) - (lanesIn/2);
		transform.localPosition = new Vector3(roadCenterPoint,0,0);

	}

	public GameObject getTrafficLights(){
		return theTrafficLights;
	}

	public List<GameObject> getSpawners(){

		return mySpawners;

	}

	public List<GameObject> getDecisionPoints(){

		return myDecisionPoints;

	}


}
