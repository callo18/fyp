﻿using UnityEngine;
using System.Collections;

public class TrafficLights : MonoBehaviour {


	//building
	private float lanes;
	private float distanceFromFront = .2f;

	private Transform objectModel;

	private GameObject trafficLightPrefab;

	private TrafficLightsBehaviour trafficlightsScript;

	//building

	public void setUpLights(float l){

		trafficlightsScript = this.transform.Find ("TrafficLightsModel").GetComponent<TrafficLightsBehaviour> ();

		lanes = l;

		calculateVariables ();

		resizePiece (lanes);

		setLocationOfPiece ();

		placeLights ();

	}

	void calculateVariables(){

		objectModel = this.transform.FindChild ("TrafficLightsModel");
		trafficLightPrefab = GameObject.Find ("Light");


	}

	void resizePiece (float li){
		transform.localRotation = transform.parent.transform.localRotation;
		objectModel.localScale = new Vector3 (li + .25f, 1, .4f);

	}
		

	void setLocationOfPiece(){

		transform.localPosition = new Vector3(0,0,distanceFromFront);

	}

	void placeLights(){

		float positionX = -(lanes/2) - .10f;

		for (int i = 0; i < 2; i++) {

			GameObject light = (GameObject)Instantiate (trafficLightPrefab, new Vector3 (0, 0, 0), transform.localRotation);

			//LaneOutPiece lop = light.GetComponent<LaneOutPiece> ();

			light.transform.parent = this.transform.FindChild("Lights");
			light.transform.name = "light";


			light.transform.localRotation = Quaternion.identity;
			light.transform.localPosition = new Vector3(positionX,0,0);
			positionX = (lanes/2) + .10f;

			trafficlightsScript.Lights.Add (light);

		}
	}
}
