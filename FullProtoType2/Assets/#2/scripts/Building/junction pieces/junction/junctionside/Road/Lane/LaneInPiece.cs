﻿using UnityEngine;
using System.Collections;

public class LaneInPiece : MonoBehaviour {
	

	public void buildLane(float positionX){

		positionLane (positionX);

	}

	void positionLane(float positionX){
		transform.localRotation = Quaternion.identity;
		transform.localPosition = new Vector3(positionX,0,0);

	}
}
