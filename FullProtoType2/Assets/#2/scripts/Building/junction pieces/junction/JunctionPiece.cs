﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class JunctionPiece : MonoBehaviour {

	// Variables for piece.
	private float northInAmount, northOutAmount, eastInAmount, eastOutAmount, southInAmount, southOutAmount, westInAmount, westOutAmount;
	private float northSize, eastSize, southSize, westSize;
	private float centerPointX,centerPointZ,centerPointY;
	private float largestHeight, largestWidth;

	//Graphics model of piece.
	private Transform objectModel;

	//The width for the island prefab.
	private float sizeOfIsland = .4f;


	//The width for the path prefab.
	private float widthOfPath = .3f;


	//The side prefab.
	private GameObject junctionSidePrefab;

	//The path prefab.
	private GameObject pathPrefab, fillerPathCornerPrefab ,fillerPathMiddlePrefab, junctionCornerPathPrefab;

	//Sorting
	//The side prefab.
	private GameObject NJunctionSidePiece,EJunctionSidePiece,SJunctionSidePiece,WJunctionSidePiece;

	public GameObject NLights,ELights,SLights,WLights;
	public List<GameObject> NSpawners,ESpawners,SSpawners,WSpawners,NCollectors,ECollectors,SCollectors,WCollectors;

	public List<GameObject> NDecisionPointers;
	public List<GameObject> EDecisionPointers;
	public List<GameObject> SDecisionPointers;
	public List<GameObject> WDecisionPointers;

	public List<GameObject> NDestinationPointers;
	public List<GameObject> EDestinationPointers;
	public List<GameObject> SDestinationPointers;
	public List<GameObject> WDestinationPointers;

	//The pieces main function which constructs and sets up everything for the piece.
	public void buildJunction(float NI, float NO,float EI, float EO, float SI, float SO,float WI, float WO){

		//set the parameters as variables.
		northInAmount = NI;
		northOutAmount = NO;
		eastInAmount = EI;
		eastOutAmount = EO;
		southInAmount = SI;
		southOutAmount = SO;
		westInAmount = WI;
		westOutAmount = WO;

		//Calculates all the necessary variables or parts that are required in the program.
		calculateVariables ();

		//Positions the object at its desired location.
		setLocationOfPiece (0,0,0);

		//Resizes the model of this piece.
		resizePiece (largestWidth,largestHeight);

		//Instatiates the sides and paths.
		placeJunctionSides ();

		builsJunctionCornerPaths ();
	}

	void setLocationOfPiece(float cpx, float cpy, float cpz){
		centerPointX = cpx;
		centerPointY= cpy;
		centerPointZ = cpz;

		objectModel.position = new Vector3 (centerPointX, centerPointY, centerPointZ);
	}

	//Resizes the graphics model of this piece.
	void resizePiece(float largestWidth, float largestHeight){
		
		Transform go = this.transform.FindChild ("JunctionModel");
		go.transform.localScale = new Vector3 (largestWidth, 0.1f, largestHeight);

	}

	void calculateVariables(){

		// Finding  the graphics model for this object 
		objectModel = this.transform.FindChild ("JunctionModel");

		// calculate the amount of lanes that will be on each side of the junction adding the island size aswell
		if (northInAmount > 0 && northOutAmount > 0) {
			
			northSize = northInAmount + northOutAmount + sizeOfIsland;
		} else {
			
			northSize = northInAmount + northOutAmount;
		
		}

		if (southInAmount > 0 && southOutAmount > 0) {
			
			southSize = southInAmount + southOutAmount + sizeOfIsland;

		} else {

			southSize = southInAmount + southOutAmount;

		}

		if (eastInAmount > 0 && eastOutAmount > 0) {
			
			eastSize = eastInAmount + eastOutAmount + sizeOfIsland;

		} else {

			eastSize = eastInAmount + eastOutAmount;

		}

		if (westInAmount > 0 && westOutAmount > 0) {
			
			westSize = westInAmount + westOutAmount + sizeOfIsland;

		} else {

			westSize = westInAmount + westOutAmount;

		}

		// calculating the dimensions of the junction

		if (northSize > southSize) {
			
			largestWidth = northSize;

		} else {
			
			largestWidth = southSize;

		}
		if (eastSize > westSize) {
			
			largestHeight = eastSize;

		} else {
			
			largestHeight = westSize;

		}

		// finding the prefabs

		junctionSidePrefab = GameObject.Find("JunctionSide");
		fillerPathCornerPrefab= GameObject.Find("PathFillerCorner");
		fillerPathMiddlePrefab = GameObject.Find("PathFillerMiddle");
		junctionCornerPathPrefab = GameObject.Find("junctionCornerPath");

	}

	void placeJunctionSides(){
		
		//if there is no lanes on this side of junction build a path instead.
		if (northSize == 0) {

			buildThePathOnSide (0, 0, (largestHeight / 2) + widthOfPath / 2, Quaternion.Euler (0, 0, 0), largestWidth); // place the new path at the top of the junction and the + .1f to account for the width of the path.
			NJunctionSidePiece = null;
		}
		else{
			

			//creates new object.
			GameObject side1 = (GameObject) Instantiate (junctionSidePrefab, new Vector3 (0, 0, largestHeight / 2), Quaternion.Euler (0, 0, 0));
			JunctionSidePiece jsp1 = side1.GetComponent<JunctionSidePiece> ();

			NJunctionSidePiece = side1;
			//Sets up new object.
			jsp1.transform.parent = this.transform.FindChild ("JunctionSides");
			jsp1.name = "JunctionSideNorth";
			jsp1.setUpSide (northInAmount,northOutAmount,northSize);

			//check should you place fillerpaths
			if(southSize>northSize){
				buildFillerPaths (side1, southSize,northSize);
			}

		}
		if (eastSize == 0) {

			buildThePathOnSide ((largestWidth / 2) + widthOfPath / 2, 0, 0, Quaternion.Euler (0, 90, 0), largestHeight);
			EJunctionSidePiece = null;
		}
		else {
			//creates new object.
			GameObject side2 = (GameObject) Instantiate (junctionSidePrefab, new Vector3 (largestWidth / 2, 0, 0), Quaternion.Euler (0, 90, 0));
			JunctionSidePiece jsp2 = side2.GetComponent<JunctionSidePiece> ();

			EJunctionSidePiece = side2;
			//Sets up new object.
			jsp2.transform.parent = this.transform.FindChild ("JunctionSides");
			jsp2.name = "JunctionSideEast";
			jsp2.setUpSide (eastInAmount,eastOutAmount,eastSize);

			//check should you place fillerpaths
			if(westSize>eastSize){
				buildFillerPaths (side2, westSize,eastSize);
			}

		}

		if (southSize == 0) {
		
			buildThePathOnSide (0, 0, -((largestHeight / 2) + widthOfPath / 2), Quaternion.Euler (0, 180, 0), largestWidth);
			SJunctionSidePiece = null;
		}
		else {
			//creates new object.
			GameObject side3 = (GameObject) Instantiate (junctionSidePrefab, new Vector3 (0, 0, -(largestHeight / 2)), Quaternion.Euler (0, 180, 0));
			JunctionSidePiece jsp3 = side3.GetComponent<JunctionSidePiece> ();

			SJunctionSidePiece = side3;
			//Sets up new object.
			jsp3.transform.parent = this.transform.FindChild ("JunctionSides");
			jsp3.name = "JunctionSideSouth";
			jsp3.setUpSide (southInAmount,southOutAmount,southSize);

			//check should you place fillerpaths
			if(northSize>southSize){
				buildFillerPaths (side3, northSize,southSize);
			}
		}

		if (westSize == 0) {
		
			buildThePathOnSide (-((largestWidth / 2) + widthOfPath / 2), 0, 0, Quaternion.Euler (0, 270, 0), largestHeight);
			WJunctionSidePiece = null;
		}
		else {
			//creates new object.
			GameObject side4 = (GameObject) Instantiate (junctionSidePrefab, new Vector3 (-(largestWidth / 2), 0, 0), Quaternion.Euler (0, 270, 0));
			JunctionSidePiece jsp4 = side4.GetComponent<JunctionSidePiece> ();

			WJunctionSidePiece = side4;
			//Sets up new object.
			jsp4.transform.parent = this.transform.FindChild ("JunctionSides");
			jsp4.name = "JunctionSideWest";
			jsp4.setUpSide (westInAmount,westOutAmount,westSize);

			//check should you place fillerpaths
			if(eastSize>westSize){
				buildFillerPaths (side4, eastSize,westSize);
			}
		}
		sortNeededPieces ();
	}

	//sets a path and changes its parent.
	void buildThePathOnSide(float cpx, float cpy, float cpz, Quaternion q, float l){
		
		GameObject path = (GameObject)Instantiate(fillerPathMiddlePrefab,new Vector3 (cpx, cpy, cpz), q);
		path.transform.localScale = new Vector3 (l+.4f, 1, 1);
		path.transform.parent = this.transform.FindChild("Paths");

	}

	void buildFillerPaths(GameObject s, float bigSide, float smallSide){

		float objectWidth = (bigSide - smallSide) / 2;
		float moveXUnits = (objectWidth / 2)-.2f;

		GameObject path = (GameObject)Instantiate(fillerPathCornerPrefab,s.transform.position, s.transform.rotation);
		path.transform.localScale = new Vector3 (objectWidth, 1, 1);
		path.transform.parent = s.transform.FindChild("Paths");
		path.transform.localPosition = new Vector3 (-(bigSide / 2) + moveXUnits , 0, .2f);

		GameObject path2 = (GameObject)Instantiate(fillerPathCornerPrefab,s.transform.position, s.transform.rotation);
		path2.transform.localScale = new Vector3 (objectWidth, 1, 1);
		path2.transform.parent = s.transform.FindChild("Paths");
		path2.transform.localPosition = new Vector3 ((bigSide / 2) - moveXUnits , 0, .2f);
	}

	void builsJunctionCornerPaths(){
		GameObject path3 = (GameObject)Instantiate(junctionCornerPathPrefab,new Vector3((largestWidth/2)+.6f,0,(largestHeight/2)+.6f), Quaternion.identity);
		path3.transform.localScale = new Vector3 (1.2f, .1f, 1.2f);
		path3.transform.parent = this.transform.FindChild("Paths");

		GameObject path4 = (GameObject)Instantiate(junctionCornerPathPrefab,new Vector3((largestWidth/2)+.6f,0,-(largestHeight/2)-.6f), Quaternion.identity);
		path4.transform.localScale = new Vector3 (1.2f, .1f, 1.2f);
		path4.transform.parent = this.transform.FindChild("Paths");

		GameObject path5 = (GameObject)Instantiate(junctionCornerPathPrefab,new Vector3(-(largestWidth/2)-.6f,0,(largestHeight/2)+.6f), Quaternion.identity);
		path5.transform.localScale = new Vector3 (1.2f, .1f, 1.2f);
		path5.transform.parent = this.transform.FindChild("Paths");

		GameObject path6 = (GameObject)Instantiate(junctionCornerPathPrefab,new Vector3(-(largestWidth/2)-.6f,0,-(largestHeight/2)-.6f), Quaternion.identity);
		path6.transform.localScale = new Vector3 (1.2f, .1f, 1.2f);
		path6.transform.parent = this.transform.FindChild("Paths");


	}

	public void sortNeededPieces(){
		
		if(NJunctionSidePiece!= null){
			NLights = NJunctionSidePiece.GetComponent<JunctionSidePiece>().getMyTrafficLights();
			NSpawners = NJunctionSidePiece.GetComponent<JunctionSidePiece> ().getMySpawners ();
			NCollectors = NJunctionSidePiece.GetComponent<JunctionSidePiece> ().getMyCollectors ();
			NDecisionPointers = NJunctionSidePiece.GetComponent<JunctionSidePiece> ().getMyDecisionPoints ();
			NDestinationPointers = NJunctionSidePiece.GetComponent<JunctionSidePiece> ().getMyDestinationPoints();
		}

		if(EJunctionSidePiece!= null){
			ELights = EJunctionSidePiece.GetComponent<JunctionSidePiece>().getMyTrafficLights();
			ESpawners = EJunctionSidePiece.GetComponent<JunctionSidePiece> ().getMySpawners ();
			ECollectors = EJunctionSidePiece.GetComponent<JunctionSidePiece> ().getMyCollectors ();
			EDecisionPointers = EJunctionSidePiece.GetComponent<JunctionSidePiece> ().getMyDecisionPoints ();
			EDestinationPointers = EJunctionSidePiece.GetComponent<JunctionSidePiece> ().getMyDestinationPoints();
		}

		if(SJunctionSidePiece!= null){
			SLights = SJunctionSidePiece.GetComponent<JunctionSidePiece>().getMyTrafficLights();
			SSpawners = SJunctionSidePiece.GetComponent<JunctionSidePiece> ().getMySpawners ();
			SCollectors = SJunctionSidePiece.GetComponent<JunctionSidePiece> ().getMyCollectors ();
			SDecisionPointers = SJunctionSidePiece.GetComponent<JunctionSidePiece> ().getMyDecisionPoints ();
			SDestinationPointers = SJunctionSidePiece.GetComponent<JunctionSidePiece> ().getMyDestinationPoints();
		}

		if(WJunctionSidePiece!= null){
			WLights = WJunctionSidePiece.GetComponent<JunctionSidePiece>().getMyTrafficLights();
			WSpawners = WJunctionSidePiece.GetComponent<JunctionSidePiece> ().getMySpawners ();
			WCollectors = WJunctionSidePiece.GetComponent<JunctionSidePiece> ().getMyCollectors ();
			WDecisionPointers = WJunctionSidePiece.GetComponent<JunctionSidePiece> ().getMyDecisionPoints ();
			WDestinationPointers = WJunctionSidePiece.GetComponent<JunctionSidePiece> ().getMyDestinationPoints();
		}
	}
}
