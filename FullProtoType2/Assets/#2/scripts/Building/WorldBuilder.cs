﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WorldBuilder : MonoBehaviour {

	public GameObject junction;
	private GameObject junctionClone;
	private JunctionPiece jp;
	public GameObject cameraController;
	private GameObject cameraControllerClone;
	private CameraController cc;
	private bool reset = true;

	public GameObject ui;

	public GameObject gameController;

	public void makeJunction(){

		if (reset) {
			
			//Get values from the gui.
			int NI = int.Parse (GameObject.Find ("NIn").GetComponent<InputField> ().text);
			int NOUT = int.Parse (GameObject.Find ("NOut").GetComponent<InputField> ().text);
			int EI = int.Parse (GameObject.Find ("EIn").GetComponent<InputField> ().text);
			int EOUT = int.Parse (GameObject.Find ("EOut").GetComponent<InputField> ().text);
			int SI = int.Parse (GameObject.Find ("SIn").GetComponent<InputField> ().text);
			int SOUT = int.Parse (GameObject.Find ("SOut").GetComponent<InputField> ().text);
			int WI = int.Parse (GameObject.Find ("WIn").GetComponent<InputField> ().text);
			int WOUT = int.Parse (GameObject.Find ("WOut").GetComponent<InputField> ().text);

			// ENTER CHECKS FOR VALID JUNCTION

			junctionClone = (GameObject)Instantiate (junction, new Vector3 (0, -10, 0), Quaternion.Euler (0, 0, 0));
			jp = junctionClone.GetComponent<JunctionPiece> ();

			cameraControllerClone = (GameObject)Instantiate (cameraController, new Vector3 (0, -10, 0), Quaternion.Euler (0, 0, 0));
			cc = cameraControllerClone.GetComponent<CameraController> ();

			cameraControllerClone.transform.parent = junctionClone.transform;

			jp.buildJunction (NI, NOUT, EI, EOUT, SI, SOUT, WI, WOUT);
			cc.getCameras ();
			reset = false;

			gameController.GetComponent<GameController> ().SetUpGame ();

		}else {
			print ("You must reset the scene before creating a new one.");
		}

	}

	public void makeDefaultWorld(){

		if (reset) {

			junctionClone = (GameObject)Instantiate (junction, new Vector3 (0, -10, 0), Quaternion.Euler (0, 0, 0));
			jp = junctionClone.GetComponent<JunctionPiece> ();

			cameraControllerClone = (GameObject)Instantiate (cameraController, new Vector3 (0, -10, 0), Quaternion.Euler (0, 0, 0));
			cc = cameraControllerClone.GetComponent<CameraController> ();

			cameraControllerClone.transform.parent = junctionClone.transform;

			jp.buildJunction (Random.Range(0,6), Random.Range(0,6), Random.Range(0,6), Random.Range(0,6), Random.Range(0,6), Random.Range(0,6), Random.Range(0,6), Random.Range(0,6));
			cc.getCameras ();
			reset = false;

			gameController.GetComponent<GameController> ().SetUpGame ();
		
		} else {
			print ("You must reset the scene before creating a new one.");
		}
	}

	public void deleteJunction(){
		if (GameObject.Find ("Junction(Clone)") != null) {
			Destroy (junctionClone);
		}
	}

	public void resetJunction(){
		if (!reset) {
			deleteJunction ();
			reset = true;
		}
	}
}
