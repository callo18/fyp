﻿using UnityEngine;
using System.Collections;

public class TheWorld : MonoBehaviour {
	public GameObject junction;
	private JunctionPiece jp;
	// Use this for initialization
	void Start () {
		jp = junction.GetComponent<JunctionPiece> ();
		jp.buildJunction (0, 0, 3, 2, 3, 5, 4, 2);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
