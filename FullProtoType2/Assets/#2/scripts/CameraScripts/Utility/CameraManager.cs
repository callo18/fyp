﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CameraManager{

	//Arrays of the cara components and camera objects
	private GameObject[] startingSecurityCamerasObjects;
	private List<Camera> securityCameras = new List<Camera>();


	//The security camera we are currently on
	public Camera chosenSecurityCamera;
	//The main camera of the scene
	public Camera mainCamera;

	//This is an integer referring to the camera it is on or should go back to.
	private int currentCamera = -1;

	public CameraManager(){
		startingSecurityCamerasObjects = GameObject.FindGameObjectsWithTag("Camera");
		startingSecurityCamerasObjects [0].GetComponent<Camera> ().enabled = false;
	}

	public void getCameras(){

		//sets all the camera components from the camera objects into an array
		securityCameras.Clear ();
		startingSecurityCamerasObjects = GameObject.FindGameObjectsWithTag("Camera");

		if (startingSecurityCamerasObjects.Length > 0) {
			for (int i = 0; i < startingSecurityCamerasObjects.Length; i++) {
				securityCameras.Add (startingSecurityCamerasObjects [i].GetComponent<Camera> ());
			}

			//disables all of the security cameras to begin with
			foreach (Camera camera in securityCameras) {
				camera.enabled = false;
			}
			chosenSecurityCamera = securityCameras[0];

		}
		//sets the variables there chosen 
		mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
	}

	//This will increase the current camera to the next one in the array.
	public void changeToNextSecurityCamera(){
		if (startingSecurityCamerasObjects.Length > 0) {
			//turn off main camera
			mainCamera.enabled = false;

			// Check if the camera index is outOfBounds
			if (currentCamera < 0 || currentCamera >= startingSecurityCamerasObjects.Length - 1) {
				currentCamera = 1;
			} else {
				currentCamera++;
			}

			//Turns off current camera then changes to new camera and turns it on
			chosenSecurityCamera.enabled = false;
			chosenSecurityCamera = securityCameras [currentCamera];
			chosenSecurityCamera.enabled = true;
		} else {
			Debug.LogError ("There are no cameras in the scene");
		}
	}

	//Switches to the main camera in the scene
	public void ChangeToMainCamera(){

		if (startingSecurityCamerasObjects.Length > 0) {
			chosenSecurityCamera.enabled = false;
		}
		mainCamera.enabled = true;
	}

}