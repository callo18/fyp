﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	public CameraManager cameraManager;
	private bool camerasWorking = false;
	public bool mainCameraOn = true;
	public bool editorMode = false;
	public bool simMode = false;

	private float height;
	private float speed = .3f;
	private float moveDistance = 12f;
	// Update is called once per frame
	void Update () {
		if (camerasWorking) {
			if (simMode) {
			
				if (Input.GetKeyDown (KeyCode.S)) {
					cameraManager.changeToNextSecurityCamera ();
					mainCameraOn = false;
				}
				if (Input.GetKeyDown (KeyCode.C)) {
					cameraManager.ChangeToMainCamera ();
					mainCameraOn = true;
				}

				if (mainCameraOn) {
					float d = Input.GetAxis ("Mouse ScrollWheel");
					if (d > 0f && height < 50) {
						height = height + d;
					} else if (d < 0f && height > 5) {
						height = height + d;
					}
					if(Input.GetKey(KeyCode.RightArrow) && cameraManager.mainCamera.transform.position.x <= moveDistance)
					{
						cameraManager.mainCamera.transform.Translate(new Vector3(speed,0,0));
					}
					if(Input.GetKey(KeyCode.LeftArrow)&& cameraManager.mainCamera.transform.position.x >= -moveDistance)
					{
						cameraManager.mainCamera.transform.Translate(new Vector3(-speed,0,0));
					}
					if(Input.GetKey(KeyCode.DownArrow)&& cameraManager.mainCamera.transform.position.z >= -moveDistance)
					{
						cameraManager.mainCamera.transform.Translate(new Vector3(0,-speed,0));
					}
					if(Input.GetKey(KeyCode.UpArrow)&& cameraManager.mainCamera.transform.position.z <= moveDistance)
					{
						cameraManager.mainCamera.transform.Translate(new Vector3(0,speed,0));
					}
					cameraManager.mainCamera.transform.position = new Vector3 (cameraManager.mainCamera.transform.position.x, height, cameraManager.mainCamera.transform.position.z);
				} else {
					mainCameraOn = true;
				}	
			}
			if (editorMode) {
				if (mainCameraOn) {
					float d = Input.GetAxis ("Mouse ScrollWheel");
					if (d > 0f && height < 50) {
						height = height + d;
					} else if (d < 0f && height > 5) {
						height = height + d;
					}if(Input.GetKey(KeyCode.RightArrow) && cameraManager.mainCamera.transform.position.x <= moveDistance)
					{
						cameraManager.mainCamera.transform.Translate(new Vector3(speed,0,0));
					}
					if(Input.GetKey(KeyCode.LeftArrow)&& cameraManager.mainCamera.transform.position.x >= -moveDistance)
					{
						cameraManager.mainCamera.transform.Translate(new Vector3(-speed,0,0));
					}
					if(Input.GetKey(KeyCode.DownArrow)&& cameraManager.mainCamera.transform.position.z >= -moveDistance)
					{
						cameraManager.mainCamera.transform.Translate(new Vector3(0,-speed,0));
					}
					if(Input.GetKey(KeyCode.UpArrow)&& cameraManager.mainCamera.transform.position.z <= moveDistance)
					{	
						cameraManager.mainCamera.transform.Translate(new Vector3(0,speed,0));
					}
					cameraManager.mainCamera.transform.position = new Vector3 (cameraManager.mainCamera.transform.position.x, height, cameraManager.mainCamera.transform.position.z);
				} else {
					mainCameraOn = true;
				}	
			}
		}
	}

	public void getCameras(){
		cameraManager = new CameraManager();
		height = 15;
		cameraManager.getCameras ();
		camerasWorking = true;
	}
}
