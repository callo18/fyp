﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PreGameSettingManager : MonoBehaviour {

	private WorldObjectsManagerScript worldObjectsManagerScript;
	private GUIManagerScript guiManagerScript;
	public TrafficLightSequenceManager trafficLightSequenceManager;

	public List<GameObject> NSpawners;
	public List<GameObject> ESpawners;
	public List<GameObject> SSpawners;
	public List<GameObject> WSpawners;

//	public GameObject NLights,ELights,SLights,WLights;

	public float NSpawnSpeed,ESpawnSpeed,SSpawnSpeed,WSpawnSpeed;
	public bool NSpawnOn,ESpawnOn,SSpawnOn,WSpawnOn;
	public int NSpeed, ESpeed, SSpeed, WSpeed;

	//Finds all the spawners and assigns them to their correct lists.
	public void Start(){
		worldObjectsManagerScript = GameObject.Find ("WorldObjectsManager").GetComponent<WorldObjectsManagerScript> ();
		guiManagerScript = GameObject.Find ("GUIManager").GetComponent<GUIManagerScript> ();
		trafficLightSequenceManager = GameObject.Find ("TrafficLightSequenceManager").GetComponent<TrafficLightSequenceManager> ();
	}

	public void setUp() {

		NSpawners = worldObjectsManagerScript.NSpawners;
		ESpawners = worldObjectsManagerScript.ESpawners;
		SSpawners = worldObjectsManagerScript.SSpawners;
		WSpawners = worldObjectsManagerScript.WSpawners;
	}

	public void ApplyEditLanesSettings(){
		
		setUpPotentialRoadBlock ();
		setUpRoadSigns ();

	}
	public void ApplySettings (){
		getSettings ();
		applySpawnerSettings ();
		setSpeedLimitInEachLane ();
		trafficLightSequenceManager.applyLightSequencingSettings ();
	}

	public void applySpawnerSettings(){
		setSettingsForEachSpawner ();

	}

	public void getSettings(){
		NSpawnSpeed = guiManagerScript.preGameSettings.transform.Find ("SpawnerControl").Find ("SpawnerSettings").Find ("N").Find ("Slider").GetComponent<Slider> ().value;
		ESpawnSpeed = guiManagerScript.preGameSettings.transform.Find ("SpawnerControl").Find ("SpawnerSettings").Find ("E").Find ("Slider").GetComponent<Slider> ().value;
		SSpawnSpeed = guiManagerScript.preGameSettings.transform.Find ("SpawnerControl").Find ("SpawnerSettings").Find ("S").Find ("Slider").GetComponent<Slider> ().value;
		WSpawnSpeed = guiManagerScript.preGameSettings.transform.Find ("SpawnerControl").Find ("SpawnerSettings").Find ("W").Find ("Slider").GetComponent<Slider> ().value;

		NSpawnOn = guiManagerScript.preGameSettings.transform.Find ("SpawnerControl").Find ("SpawnerSettings").Find ("N").Find ("Toggle").GetComponent<Toggle> ().isOn;
		ESpawnOn = guiManagerScript.preGameSettings.transform.Find ("SpawnerControl").Find ("SpawnerSettings").Find ("E").Find ("Toggle").GetComponent<Toggle> ().isOn;
		SSpawnOn = guiManagerScript.preGameSettings.transform.Find ("SpawnerControl").Find ("SpawnerSettings").Find ("S").Find ("Toggle").GetComponent<Toggle> ().isOn;
		WSpawnOn = guiManagerScript.preGameSettings.transform.Find ("SpawnerControl").Find ("SpawnerSettings").Find ("W").Find ("Toggle").GetComponent<Toggle> ().isOn;

		NSpeed = int.Parse (guiManagerScript.preGameSettings.transform.Find ("SpeedLimitControl").Find ("SpeedSettings").Find ("N").Find ("InputField").GetComponent<InputField> ().text);
		ESpeed = int.Parse (guiManagerScript.preGameSettings.transform.Find ("SpeedLimitControl").Find ("SpeedSettings").Find ("E").Find ("InputField").GetComponent<InputField> ().text);
		SSpeed = int.Parse (guiManagerScript.preGameSettings.transform.Find ("SpeedLimitControl").Find ("SpeedSettings").Find ("S").Find ("InputField").GetComponent<InputField> ().text);
		WSpeed = int.Parse (guiManagerScript.preGameSettings.transform.Find ("SpeedLimitControl").Find ("SpeedSettings").Find ("W").Find ("InputField").GetComponent<InputField> ().text);

	}

	public void setSettingsForEachSpawner(){

		foreach (GameObject spawner in NSpawners) {
			spawner.GetComponent<VehicleSpawn> ().setSpawnRate (NSpawnSpeed);
		}

		foreach (GameObject spawner in ESpawners) {
			spawner.GetComponent<VehicleSpawn> ().setSpawnRate (ESpawnSpeed);
		}

		foreach (GameObject spawner in SSpawners) {
			spawner.GetComponent<VehicleSpawn> ().setSpawnRate (SSpawnSpeed);
		}

		foreach (GameObject spawner in WSpawners) {
			spawner.GetComponent<VehicleSpawn> ().setSpawnRate (WSpawnSpeed);
		}
	}

	public void setUpPotentialRoadBlock(){

		foreach (GameObject spawner in NSpawners) {
			spawner.GetComponent<VehicleSpawn> ().checkForPossibleDestination ();
		}

		foreach (GameObject spawner in ESpawners) {
			spawner.GetComponent<VehicleSpawn> ().checkForPossibleDestination ();
		}

		foreach (GameObject spawner in SSpawners) {
			spawner.GetComponent<VehicleSpawn> ().checkForPossibleDestination ();
		}

		foreach (GameObject spawner in WSpawners) {
			spawner.GetComponent<VehicleSpawn> ().checkForPossibleDestination ();
		}
	}

	public void setSpeedLimitInEachLane(){

		foreach (GameObject spawner in NSpawners) {
			spawner.GetComponent<VehicleSpawn> ().setSpeedLimit (NSpeed);
		}

		foreach (GameObject spawner in ESpawners) {
			spawner.GetComponent<VehicleSpawn> ().setSpeedLimit (ESpeed);
		}

		foreach (GameObject spawner in SSpawners) {
			spawner.GetComponent<VehicleSpawn> ().setSpeedLimit (SSpeed);
		}

		foreach (GameObject spawner in WSpawners) {
			spawner.GetComponent<VehicleSpawn> ().setSpeedLimit (WSpeed);
		}
	}

	public void setUpRoadSigns(){

		foreach (GameObject spawner in NSpawners) {
			spawner.GetComponent<VehicleSpawn> ().setUpRoadSign();
		}

		foreach (GameObject spawner in ESpawners) {
			spawner.GetComponent<VehicleSpawn> ().setUpRoadSign();
		}

		foreach (GameObject spawner in SSpawners) {
			spawner.GetComponent<VehicleSpawn> ().setUpRoadSign();
		}

		foreach (GameObject spawner in WSpawners) {
			spawner.GetComponent<VehicleSpawn> ().setUpRoadSign();
		}

	}

	public void resetLists(){

		NSpawners.Clear();
		ESpawners.Clear();
		SSpawners.Clear();
		WSpawners.Clear();
	}
}
