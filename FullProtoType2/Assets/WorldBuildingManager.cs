﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WorldBuildingManager : MonoBehaviour {

	//JUNCTION PIECES
	private GameObject junction;
	private GameObject junctionClone;
	private JunctionPiece jp;

	//NEEDED VARIABLES
	WorldObjectsManagerScript worldObjectsManagerScript;
	EditingLanesManager editingLanesManager;

	//MIGHT BE DELETED
	//public GameObject cameraController;
	//private GameObject cameraControllerClone;

	void Start(){

		worldObjectsManagerScript = GameObject.Find ("WorldObjectsManager").GetComponent<WorldObjectsManagerScript> ();
		editingLanesManager = GameObject.Find ("EditingLanesManager").GetComponent<EditingLanesManager> ();
		junction = GameObject.Find ("Junction");
	}

	public void makeJunction(){

		// ENSURE NO DOUBLE JUNCTIONS
		deleteJunction();
		// RETRIEVE THE INPUTS FROM THE GUI
		int NI = int.Parse (GameObject.Find ("NIn").GetComponent<InputField> ().text);
		int NOUT = int.Parse (GameObject.Find ("NOut").GetComponent<InputField> ().text);
		int EI = int.Parse (GameObject.Find ("EIn").GetComponent<InputField> ().text);
		int EOUT = int.Parse (GameObject.Find ("EOut").GetComponent<InputField> ().text);
		int SI = int.Parse (GameObject.Find ("SIn").GetComponent<InputField> ().text);
		int SOUT = int.Parse (GameObject.Find ("SOut").GetComponent<InputField> ().text);
		int WI = int.Parse (GameObject.Find ("WIn").GetComponent<InputField> ().text);
		int WOUT = int.Parse (GameObject.Find ("WOut").GetComponent<InputField> ().text);

		// ENTER CHECKS FOR VALID JUNCTION TODO:

		// CREATING AND PREPARING JUNCTION PIECE
		junctionClone = (GameObject)Instantiate (junction, new Vector3 (0, -10, 0), Quaternion.Euler (0, 0, 0));
		jp = junctionClone.GetComponent<JunctionPiece> ();

		// BUILDING JUNCTION USING THE NECESSARY PARAMETERS
		jp.buildJunction (NI, NOUT, EI, EOUT, SI, SOUT, WI, WOUT);
	}

	public void makeRandomWorld(){

		// ENSURE NO DOUBLE JUNCTIONS
		deleteJunction();

		// CREATING AND PREPARING JUNCTION PIECE
		junctionClone = (GameObject)Instantiate (junction, new Vector3 (0, -10, 0), Quaternion.Euler (0, 0, 0));
		jp = junctionClone.GetComponent<JunctionPiece> ();

		// BUILDING JUNCTION USING THE NECESSARY PARAMETERS
		jp.buildJunction (Random.Range(0,6), Random.Range(0,6), Random.Range(0,6), Random.Range(0,6), Random.Range(0,6), Random.Range(0,6), Random.Range(0,6), Random.Range(0,6));
	}

	public void make2x1World(){

		// ENSURE NO DOUBLE JUNCTIONS
		deleteJunction();
		// CREATING AND PREPARING JUNCTION PIECE
		junctionClone = (GameObject)Instantiate (junction, new Vector3 (0, -10, 0), Quaternion.Euler (0, 0, 0));
		jp = junctionClone.GetComponent<JunctionPiece> ();

		// BUILDING JUNCTION USING THE NECESSARY PARAMETERS
		jp.buildJunction (2, 1, 2, 1, 1, 2, 1, 2);
	}

	public void makeWhiteHallChurchWorld(){

		// ENSURE NO DOUBLE JUNCTIONS
		deleteJunction();
		// CREATING AND PREPARING JUNCTION PIECE
		junctionClone = (GameObject)Instantiate (junction, new Vector3 (0, -10, 0), Quaternion.Euler (0, 0, 0));
		jp = junctionClone.GetComponent<JunctionPiece> ();

		// BUILDING JUNCTION USING THE NECESSARY PARAMETERS
		jp.buildJunction (4, 1, 3, 3, 3, 1, 4, 2);

	}

	// ------------------------- Start And Finish------------------------- //

	public void reset(){
		
		deleteJunction ();
	}

	// ------------------------- Extra Functionality ------------------------- //
	public void deleteJunction(){
		
		if (GameObject.Find ("Junction(Clone)") != null) {
			Destroy (junctionClone);
		}
	}

//	public void makeRandomWorld(){ //OLD
//
//		if (reset) {
//
//			junctionClone = (GameObject)Instantiate (junction, new Vector3 (0, -10, 0), Quaternion.Euler (0, 0, 0));
//			jp = junctionClone.GetComponent<JunctionPiece> ();
//
//			//			cameraControllerClone = (GameObject)Instantiate (cameraController, new Vector3 (0, -10, 0), Quaternion.Euler (0, 0, 0));
//			//			cc = cameraControllerClone.GetComponent<CameraController> ();
//			//
//			//			cameraControllerClone.transform.parent = junctionClone.transform;
//
//			jp.buildJunction (Random.Range(0,6), Random.Range(0,6), Random.Range(0,6), Random.Range(0,6), Random.Range(0,6), Random.Range(0,6), Random.Range(0,6), Random.Range(0,6));
//			//			cc.getCameras ();
//			reset = false;
//
//			//gameController.GetComponent<GameController> ().SetUpGame ();
//
//		} else {
//			print ("You must reset the scene before creating a new one.");
//		}
//	}
}
