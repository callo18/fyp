﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DecisionPoint : MonoBehaviour {
	
	public List<GameObject> StraightDestinationPointers;
	public List<GameObject> LeftDestinationPointers;
	public List<GameObject> RightDestinationPointers;

	public GameObject sdestination,ldestination,rdestination;

	public GameObject Spawner;

	public List<GameObject> validsDestinations;
	public List<string> validsDestinationsDirections;


	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
		drawDebugLines ();
	}

	public void highlightSDestinationPoints(){
		
		foreach (GameObject dp in StraightDestinationPointers) {
			
			dp.GetComponent<Renderer>().enabled = true;
		}
	}

	public void highlightLDestinationPoints(){
		
		foreach (GameObject dp in LeftDestinationPointers) {
			
			dp.GetComponent<Renderer>().enabled = true;
		}
	}

	public void highlightRDestinationPoints(){
		
		foreach (GameObject dp in RightDestinationPointers) {
			
			dp.GetComponent<Renderer>().enabled = true;
		}
	}

	public void unhighlightSDestinationPoints(){
		
		foreach (GameObject dp in StraightDestinationPointers) {
			
			dp.GetComponent<Renderer>().enabled = false;
		}
	}

	public void unhighlightLDestinationPoints(){
		
		foreach (GameObject dp in LeftDestinationPointers) {
			
			dp.GetComponent<Renderer>().enabled = false;
		}
	}

	public void unhighlightRDestinationPoints(){
		
		foreach (GameObject dp in RightDestinationPointers) {
			
			dp.GetComponent<Renderer>().enabled = false;
		}
	}



	public void AssignDecisionPointItsChosenPossibleDirections(){

		validsDestinations.Clear ();
		validsDestinationsDirections.Clear ();

		if (ldestination != null) {
			validsDestinations.Add (ldestination);
			validsDestinationsDirections.Add ("left");
		}
		
		if (sdestination != null) {
			validsDestinations.Add (sdestination);
			validsDestinationsDirections.Add ("straight");
		}
			
		
		if (rdestination != null){
			validsDestinations.Add (rdestination);
			validsDestinationsDirections.Add ("right");
		}
	}

	public void drawDebugLines(){
		
		if (sdestination != null) {
			Debug.DrawLine (transform.position, sdestination.transform.position, Color.red);
		}

		if (ldestination != null) {
			Debug.DrawLine (transform.position, ldestination.transform.position, Color.red);
		}

		if (rdestination != null) {
			Debug.DrawLine (transform.position, rdestination.transform.position, Color.red);
		}

		if(Spawner != null){
			Debug.DrawLine (transform.position, Spawner.transform.position, Color.red);
		}
	}
}
