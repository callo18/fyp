﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EditingLanesManager : MonoBehaviour {

	private WorldObjectsManagerScript worldObjectsManagerScript;

	public List<GameObject> NDecisionPointers;
	public List<GameObject> EDecisionPointers;
	public List<GameObject> SDecisionPointers;
	public List<GameObject> WDecisionPointers;

	public List<GameObject> NDestinationPointers;
	public List<GameObject> EDestinationPointers;
	public List<GameObject> SDestinationPointers;
	public List<GameObject> WDestinationPointers;

	public void Start(){
		
		worldObjectsManagerScript = GameObject.Find ("WorldObjectsManager").GetComponent<WorldObjectsManagerScript> ();
	}
	/*-- Called by the GameManager after building the world.
	 * assignDecisionPointerAndDestinationsToList ();
	 * // Assign all the objects needed for the Path deciding eg Decision Points and Destination Points.

	* assignAllPossibleDestinationsToDecisionPoints ();
	* // Gives each Decsion Point the Possible Destination Points that it can travel to

	* assignDestinationPointsTheirCollectors ();
	* Link the Destination points to their Collectors

	* assignDecisionPointsTheirSpawners ();
	* // Link the Decision Points to their Spawners
	*/

	public void setUp() {

		assignDecisionPointerAndDestinationsToList ();
		assignAllPossibleDestinationsToDecisionPoints ();
		assignDestinationPointsTheirCollectors ();
		assignDecisionPointsTheirSpawners ();
	}

	//--
	public void assignDecisionPointerAndDestinationsToList(){

		NDecisionPointers = worldObjectsManagerScript.NDecisionPointers;
		EDecisionPointers = worldObjectsManagerScript.EDecisionPointers;
		SDecisionPointers = worldObjectsManagerScript.SDecisionPointers;
		WDecisionPointers = worldObjectsManagerScript.WDecisionPointers;

		NDestinationPointers = worldObjectsManagerScript.NDestinationPointers;
		EDestinationPointers = worldObjectsManagerScript.EDestinationPointers;
		SDestinationPointers = worldObjectsManagerScript.SDestinationPointers;
		WDestinationPointers = worldObjectsManagerScript.WDestinationPointers;
	}

	//--
	public void assignAllPossibleDestinationsToDecisionPoints(){

		foreach(GameObject dp in NDecisionPointers){

			dp.GetComponent<DecisionPoint> ().StraightDestinationPointers = SDestinationPointers;
			dp.GetComponent<DecisionPoint> ().RightDestinationPointers = WDestinationPointers;
			dp.GetComponent<DecisionPoint> ().LeftDestinationPointers = EDestinationPointers;
		}

		foreach(GameObject dp in EDecisionPointers){

			dp.GetComponent<DecisionPoint> ().StraightDestinationPointers = WDestinationPointers;
			dp.GetComponent<DecisionPoint> ().RightDestinationPointers = NDestinationPointers;
			dp.GetComponent<DecisionPoint> ().LeftDestinationPointers = SDestinationPointers;
		}

		foreach(GameObject dp in SDecisionPointers){

			dp.GetComponent<DecisionPoint> ().StraightDestinationPointers = NDestinationPointers;
			dp.GetComponent<DecisionPoint> ().RightDestinationPointers = EDestinationPointers;
			dp.GetComponent<DecisionPoint> ().LeftDestinationPointers = WDestinationPointers;
		}

		foreach(GameObject dp in WDecisionPointers){

			dp.GetComponent<DecisionPoint> ().StraightDestinationPointers = EDestinationPointers;
			dp.GetComponent<DecisionPoint> ().RightDestinationPointers = SDestinationPointers;
			dp.GetComponent<DecisionPoint> ().LeftDestinationPointers = NDestinationPointers;
		}
	}

	//--
	public void assignDestinationPointsTheirCollectors(){

		foreach(GameObject dp in NDestinationPointers){

			dp.GetComponent<DestinationPoint> ().Collector = dp.transform.parent.Find ("VehicleCollector").gameObject;
		}

		foreach(GameObject dp in EDestinationPointers){

			dp.GetComponent<DestinationPoint> ().Collector = dp.transform.parent.Find ("VehicleCollector").gameObject;
		}

		foreach(GameObject dp in SDestinationPointers){

			dp.GetComponent<DestinationPoint> ().Collector = dp.transform.parent.Find ("VehicleCollector").gameObject;
		}

		foreach(GameObject dp in WDestinationPointers){

			dp.GetComponent<DestinationPoint> ().Collector = dp.transform.parent.Find ("VehicleCollector").gameObject;
		}
	}

	public void HideDestinationPoints(){

		foreach(GameObject dp in NDestinationPointers){

			dp.GetComponent<Renderer> ().enabled = false;
		}

		foreach (GameObject dp in SDestinationPointers) {

			dp.GetComponent<Renderer> ().enabled = false;
		}
		foreach(GameObject dp in EDestinationPointers){

			dp.GetComponent<Renderer> ().enabled = false;
		}

		foreach(GameObject dp in WDestinationPointers){

			dp.GetComponent<Renderer> ().enabled = false;
		}
	}

	//--
	public void assignDecisionPointsTheirSpawners(){

		foreach(GameObject dp in NDecisionPointers){

			dp.GetComponent<DecisionPoint> ().Spawner = dp.transform.parent.Find ("VehicleSpawner").gameObject;
		}

		foreach(GameObject dp in EDecisionPointers){

			dp.GetComponent<DecisionPoint> ().Spawner = dp.transform.parent.Find ("VehicleSpawner").gameObject;
		}

		foreach(GameObject dp in SDecisionPointers){

			dp.GetComponent<DecisionPoint> ().Spawner = dp.transform.parent.Find ("VehicleSpawner").gameObject;
		}

		foreach(GameObject dp in WDecisionPointers){

			dp.GetComponent<DecisionPoint> ().Spawner = dp.transform.parent.Find ("VehicleSpawner").gameObject;
		}
	}
	/* Called by GameManager
	 * 
	 * AssignEachDecisionPointItsChosenPossibleDirections ();
	 * // After finalising the choosing lane direction phase, each decision creates a list of its available directions
	* 
	* hideDecisionPoints ();
	* // Hides the Elements of the decision points that will not be in the running simulation
	*/


	public void showDecisionPoints(){

		reopenDecisionPhase ();
	}

	public void finish(){

		AssignEachDecisionPointItsChosenPossibleDirections ();
		HideDestinationPoints ();
		hideDecisionPoints ();
	}

	//--
	public void AssignEachDecisionPointItsChosenPossibleDirections(){

		foreach(GameObject dp in NDecisionPointers){

			dp.GetComponent<DecisionPoint> ().AssignDecisionPointItsChosenPossibleDirections ();
		}

		foreach(GameObject dp in EDecisionPointers){

			dp.GetComponent<DecisionPoint> ().AssignDecisionPointItsChosenPossibleDirections ();
		}

		foreach(GameObject dp in SDecisionPointers){

			dp.GetComponent<DecisionPoint> ().AssignDecisionPointItsChosenPossibleDirections ();
		}

		foreach(GameObject dp in WDecisionPointers){

			dp.GetComponent<DecisionPoint> ().AssignDecisionPointItsChosenPossibleDirections ();
		}

	}


	//--
	public void hideDecisionPoints(){

		foreach (GameObject dp in NDecisionPointers) {

			dp.GetComponent<Renderer> ().enabled = false;
			dp.transform.FindChild ("Straight").gameObject.SetActive (false);
			dp.transform.FindChild ("Left").gameObject.SetActive (false);
			dp.transform.FindChild ("Right").gameObject.SetActive (false);
		}

		foreach (GameObject dp in EDecisionPointers) {

			dp.GetComponent<Renderer> ().enabled = false;
			dp.transform.FindChild ("Straight").gameObject.SetActive (false);
			dp.transform.FindChild ("Left").gameObject.SetActive (false);
			dp.transform.FindChild ("Right").gameObject.SetActive (false);
		}

		foreach (GameObject dp in SDecisionPointers) {

			dp.GetComponent<Renderer> ().enabled = false;
			dp.transform.FindChild ("Straight").gameObject.SetActive (false);
			dp.transform.FindChild ("Left").gameObject.SetActive (false);
			dp.transform.FindChild ("Right").gameObject.SetActive (false);
		}

		foreach (GameObject dp in WDecisionPointers) {

			dp.GetComponent<Renderer> ().enabled = false;
			dp.transform.FindChild ("Straight").gameObject.SetActive (false);
			dp.transform.FindChild ("Left").gameObject.SetActive (false);
			dp.transform.FindChild ("Right").gameObject.SetActive (false);
		}
	}

	/*
	 * 
	 * 
	 *  EXTRA
	 * 
	 * 
	 * 
	*/

	public void reopenDecisionPhase(){

		foreach (GameObject dp in NDecisionPointers) {

			dp.GetComponent<Renderer> ().enabled = true;
			dp.transform.FindChild ("Straight").gameObject.SetActive (true);
			dp.transform.FindChild ("Left").gameObject.SetActive (true);
			dp.transform.FindChild ("Right").gameObject.SetActive (true);
		}

		foreach (GameObject dp in EDecisionPointers) {

			dp.GetComponent<Renderer> ().enabled = true;
			dp.transform.FindChild ("Straight").gameObject.SetActive (true);
			dp.transform.FindChild ("Left").gameObject.SetActive (true);
			dp.transform.FindChild ("Right").gameObject.SetActive (true);
		}

		foreach (GameObject dp in SDecisionPointers) {

			dp.GetComponent<Renderer> ().enabled = true;
			dp.transform.FindChild ("Straight").gameObject.SetActive (true);
			dp.transform.FindChild ("Left").gameObject.SetActive (true);
			dp.transform.FindChild ("Right").gameObject.SetActive (true);
		}

		foreach (GameObject dp in WDecisionPointers) {

			dp.GetComponent<Renderer> ().enabled = true;
			dp.transform.FindChild ("Straight").gameObject.SetActive (true);
			dp.transform.FindChild ("Left").gameObject.SetActive (true);
			dp.transform.FindChild ("Right").gameObject.SetActive (true);
		}
	}

	public void resetLists(){

		NDecisionPointers.Clear();
		EDecisionPointers.Clear();
		SDecisionPointers.Clear();
		WDecisionPointers.Clear();

		NDestinationPointers.Clear();
		EDestinationPointers.Clear();
		SDestinationPointers.Clear();
		WDestinationPointers.Clear();
	}

	public void setPreDecidedDecisions(int NL, int NS, int NSDecisionLane, int NSDestinationLane, int NR, int EL, int ES, int ESDecisionLane, int ESDestinationLane, int ER, int SL, int SS, int SSDecisionLane, int SSDestinationLane, int SR, int WL, int WS, int WSDecisionLane, int WSDestinationLane, int WR){

		//N
		for (int i = 0; i < NL; i++) {
			NDecisionPointers [NDecisionPointers.Count-1-i].GetComponent<DecisionPoint> ().ldestination = EDestinationPointers [i];
		}
		for (int i = 0; i < NS; i++) {
			NDecisionPointers [NSDecisionLane-i].GetComponent<DecisionPoint> ().sdestination = SDestinationPointers [NSDestinationLane + i];
		}
		for (int i = 0; i < NR; i++) {
			NDecisionPointers [i].GetComponent<DecisionPoint> ().rdestination = WDestinationPointers [WDestinationPointers.Count-1-i];
		}
		//E
		for (int i = 0; i < EL; i++) {
			EDecisionPointers [EDecisionPointers.Count-1-i].GetComponent<DecisionPoint> ().ldestination = SDestinationPointers [i];
		}
		for (int i = 0; i < ES; i++) {
			EDecisionPointers [ESDecisionLane-i].GetComponent<DecisionPoint> ().sdestination = WDestinationPointers [ESDestinationLane + i];
		}
		for (int i = 0; i < ER; i++) {
			EDecisionPointers [i].GetComponent<DecisionPoint> ().rdestination = NDestinationPointers [NDestinationPointers.Count-1-i];
		}
		//S
		for (int i = 0; i < SL; i++) {
			SDecisionPointers [SDecisionPointers.Count-1-i].GetComponent<DecisionPoint> ().ldestination = WDestinationPointers [i];
		}
		for (int i = 0; i < SS; i++) {
			SDecisionPointers [SSDecisionLane-i].GetComponent<DecisionPoint> ().sdestination = NDestinationPointers [SSDestinationLane + i];
		}
		for (int i = 0; i < SR; i++) {
			SDecisionPointers [i].GetComponent<DecisionPoint> ().rdestination = EDestinationPointers [EDestinationPointers.Count-1-i];
		}
		//W
		for (int i = 0; i < WL; i++) {
			WDecisionPointers [WDecisionPointers.Count-1-i].GetComponent<DecisionPoint> ().ldestination = NDestinationPointers [i];
		}
		for (int i = 0; i < WS; i++) {
			WDecisionPointers [WSDecisionLane-i].GetComponent<DecisionPoint> ().sdestination = EDestinationPointers [WSDestinationLane + i];
		}
		for (int i = 0; i < WR; i++) {
			WDecisionPointers [i].GetComponent<DecisionPoint> ().rdestination = SDestinationPointers [SDestinationPointers.Count-1-i];
		}
	}

	public void setWhiteHallJunctionDecisionLanes(){

		setPreDecidedDecisions (1, 1, 2, 0, 2,
								1, 2, 1, 0, 1,
								1, 1, 1, 0, 1,
								1, 2, 2, 1, 1);

	}
}
