﻿using UnityEngine;
using System.Collections;

public class JunctionBuilder : MonoBehaviour {

	//prefabs
	public GameObject path,island,roadIn,roadOut,trafficLight,junctionNetwork;
	//variables
	float islandPositionX,islandPositionY;
	float RoadInPositionX,RoadInPositionY;
	float roadOutPositionX,roadOutPositionY;
	float trafficLightPositionX,trafficLightPositionY;
	float Li,Lo,Ti,To,Ri,Ro,Bi,Bo;
	float LL,LT,LR,LB;
	float ChosenWidth,ChosenHeight;
	int countedRoadsIn = 0;
	int countedRoadsOut = 0;
	int countedIslands = 0;
	int countedTrafficLights = 0;
	int countedCornerPaths = 0;
	//arrays of junction objects
	public GameObject[] trafficLights;
	public GameObject[] cornerPaths;
	public GameObject[] islands;
	public GameObject[] roadsIn;
	public GameObject[] roadsOut;
	//scripts
	NetworkTrafficLights ntl;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void BuildJunction(int tLi, int tLo, int tTi, int tTo, int tRi, int tRo, int tBi, int tBo){

		SetJunctionVariables (tLi, tLo, tTi, tTo, tRi, tRo, tBi, tBo);
		SetSizeOfJunction ();
		SetCornersOfJunction ();
		SetCenterIslandsOfJunction ();
		SetRoadsIntoJunction ();
		SetRoadsOutOfJunction ();
		SetTrafficLights ();
		makeObjectsChildOfJunction ();
	}

	void SetJunctionVariables(int tLi, int tLo, int tTi, int tTo, int tRi, int tRo, int tBi, int tBo){
		Li = tLi;
		Lo = tLo;
		Ti = tTi;
		To = tTo;
		Ri = tRi;
		Ro = tRo;
		Bi = tBi;
		Bo = tBo;

		LL = Li + .4f + Lo;
		LR = Ri + .4f + Ro;
		LT = Ti + .4f + To;
		LB = Bi + .4f + Bo;

		float trafficLightsNeeded = 0;
		float roadsInNeeded = 0;
		float roadsOutNeeded = 0;
		float islandsNeeded = 0;

		if(Li>0){
			islandsNeeded++;
			trafficLightsNeeded++;
			roadsInNeeded += Li;
		}
		if(Ti>0){
			islandsNeeded++;
			trafficLightsNeeded++;
			roadsInNeeded += Ti;
		}
		if(Ri>0){
			islandsNeeded++;
			trafficLightsNeeded++;
			roadsInNeeded += Ri;
		}
		if(Bi>0){
			islandsNeeded++;
			trafficLightsNeeded++;
			roadsInNeeded += Bi;
		}
		if(Lo>0){
			roadsOutNeeded += Lo;
		}
		if(To>0){
			roadsOutNeeded += To;
		}
		if(Ro>0){
			roadsOutNeeded += Ro;
		}
		if(Bo>0){
			roadsOutNeeded += Bo;
		}


		trafficLights = new GameObject[(int)trafficLightsNeeded];
		roadsIn = new GameObject[(int)roadsInNeeded];
		roadsOut = new GameObject[(int)roadsOutNeeded];
		islands = new GameObject[(int)islandsNeeded];
		cornerPaths = new GameObject[4];
		
	}

	void SetSizeOfJunction(){
		
		if (LL >= LR) {
			ChosenHeight = LL;
		} else {
			ChosenHeight = LR;
		}
		if (LT >= LB) {
			ChosenWidth = LT;
		} else {
			ChosenWidth = LB;
		}

		transform.localScale = new Vector3 (ChosenWidth, 0.1f, ChosenHeight);
	}

	void SetCornersOfJunction(){
		//left
		islandPositionX = -((ChosenWidth / 2) + .5f);
		islandPositionY = -((ChosenHeight / 2) + .5f);;
		cornerPaths[countedCornerPaths] = (GameObject)Instantiate (path, new Vector3 (islandPositionX, .2f, islandPositionY), Quaternion.identity);
		countedCornerPaths++;
		//top
		islandPositionX = -((ChosenWidth / 2) + .5f);
		islandPositionY = ((ChosenHeight / 2) + .5f);;
		cornerPaths[countedCornerPaths] = (GameObject)Instantiate (path, new Vector3 (islandPositionX, .2f, islandPositionY), Quaternion.identity);
		countedCornerPaths++;
		//right
		islandPositionX = ((ChosenWidth / 2) + .5f);
		islandPositionY = ((ChosenHeight / 2) + .5f);;
		cornerPaths[countedCornerPaths] = (GameObject)Instantiate (path, new Vector3 (islandPositionX, .2f, islandPositionY), Quaternion.identity);
		countedCornerPaths++;
		//bottom
		islandPositionX = ((ChosenWidth / 2) + .5f);
		islandPositionY = -((ChosenHeight / 2) + .5f);;
		cornerPaths[countedCornerPaths] = (GameObject)Instantiate (path, new Vector3 (islandPositionX, .2f, islandPositionY), Quaternion.identity);
		countedCornerPaths++;
	}

	void SetCenterIslandsOfJunction(){

		//left
		if (Li > 0) {
			islandPositionX = -((ChosenWidth / 2)+.2f);
			islandPositionY = -((Li - Lo) / 2);
			islands[countedIslands] = (GameObject)Instantiate (island, new Vector3 (islandPositionX, .2f, islandPositionY), Quaternion.identity);
			countedIslands++;
		}
		//top
		if (Ti >0) {
			islandPositionX = (To - Ti) / 2;
			islandPositionY = ((ChosenHeight / 2)+.2f);
			islands[countedIslands] = (GameObject)Instantiate (island, new Vector3 (islandPositionX, .2f, islandPositionY), Quaternion.identity);
			countedIslands++;
		}
		//right
		if (Ri > 0) {
			islandPositionX = ((ChosenWidth / 2)+.2f);
			islandPositionY = (Ri - Ro) / 2;
			islands[countedIslands] = (GameObject)Instantiate (island, new Vector3 (islandPositionX, .2f, islandPositionY), Quaternion.identity);
			countedIslands++;
		}
		//bottom
		if (Bi > 0) {
			islandPositionX = (Bi - Bo) / 2;
			islandPositionY = -((ChosenHeight / 2)+.2f);
			islands[countedIslands] = (GameObject)Instantiate (island, new Vector3 (islandPositionX, .2f, islandPositionY), Quaternion.identity);
			countedIslands++;
		}
	}

	void SetRoadsIntoJunction(){
		//left

		for (int i = 0; i < Li; i++) { // rotation should be immediate & -2.5f should be half the road in
			RoadInPositionX = -(ChosenWidth / 2) - 5f;
			RoadInPositionY = (LL / 2) -.5f - i;
			roadsIn[countedRoadsIn] = (GameObject)Instantiate (roadIn, new Vector3 (RoadInPositionX, .1f, RoadInPositionY),  Quaternion.identity);
			roadsIn[countedRoadsIn].transform.eulerAngles = new Vector3(transform.eulerAngles.x,90.0f, transform.eulerAngles.z);
			countedRoadsIn++;
		}
		//top
		for (int i = 0; i < Ti; i++) {
			RoadInPositionX = (LT / 2) -.5f - i;
			RoadInPositionY = (ChosenHeight / 2) + 5f;
			roadsIn[countedRoadsIn] = (GameObject)Instantiate (roadIn, new Vector3 (RoadInPositionX, .1f, RoadInPositionY),  Quaternion.identity);
			roadsIn[countedRoadsIn].transform.eulerAngles = new Vector3(transform.eulerAngles.x,180.0f, transform.eulerAngles.z);
			countedRoadsIn++;
		}
		//right
		for (int i = 0; i < Ri; i++) {
			RoadInPositionX = (ChosenWidth / 2) + 5f;
			RoadInPositionY = -(LR / 2) +.5f + i;
			roadsIn[countedRoadsIn] = (GameObject)Instantiate (roadIn, new Vector3 (RoadInPositionX, .1f, RoadInPositionY),  Quaternion.identity);
			roadsIn[countedRoadsIn].transform.eulerAngles = new Vector3(transform.eulerAngles.x,270.0f, transform.eulerAngles.z);
			countedRoadsIn++;
		}
		//bottom
		for (int i = 0; i < Bi; i++) {
			RoadInPositionX = -(LB / 2) +.5f + i;
			RoadInPositionY = -(ChosenHeight / 2) - 5f;
			roadsIn[countedRoadsIn] = (GameObject)Instantiate (roadIn, new Vector3 (RoadInPositionX, .1f, RoadInPositionY),  Quaternion.identity);
			roadsIn[countedRoadsIn].transform.eulerAngles = new Vector3(transform.eulerAngles.x,0f, transform.eulerAngles.z);
			countedRoadsIn++;
		}
	}

	void SetRoadsOutOfJunction(){
		//left
		for (int i = 0; i < Lo; i++) { // rotation should be immediate & -2.5f should be half the road in
			roadOutPositionX = -(ChosenWidth / 2) - 5f;
			roadOutPositionY = -(LL / 2) +.5f + i;
			roadsOut[countedRoadsOut] = (GameObject)Instantiate (roadOut, new Vector3 (roadOutPositionX, .1f, roadOutPositionY),  Quaternion.identity);
			roadsOut[countedRoadsOut].transform.eulerAngles = new Vector3(transform.eulerAngles.x,90.0f, transform.eulerAngles.z);
			countedRoadsOut++;
		}
		//top
		for (int i = 0; i < To; i++) {
			roadOutPositionX = -(LT / 2) +.5f + i;
			roadOutPositionY = (ChosenHeight / 2) + 5f;
			roadsOut[countedRoadsOut] = (GameObject)Instantiate (roadOut, new Vector3 (roadOutPositionX, .1f, roadOutPositionY),  Quaternion.identity);
			roadsOut[countedRoadsOut].transform.eulerAngles = new Vector3(transform.eulerAngles.x,180.0f, transform.eulerAngles.z);
			countedRoadsOut++;
		}
		//right
		for (int i = 0; i < Ro; i++) {
			roadOutPositionX = (ChosenWidth / 2) + 5f;
			roadOutPositionY = (LR / 2) -.5f - i;
			roadsOut[countedRoadsOut] = (GameObject)Instantiate (roadOut, new Vector3 (roadOutPositionX, .1f, roadOutPositionY),  Quaternion.identity);
			roadsOut[countedRoadsOut].transform.eulerAngles = new Vector3(transform.eulerAngles.x,270.0f, transform.eulerAngles.z);
			countedRoadsOut++;
		}
		//bottom
		for (int i = 0; i < Bo; i++) {
			roadOutPositionX = (LB / 2) -.5f - i;
			roadOutPositionY = -(ChosenHeight / 2) - 5f;
			roadsOut[countedRoadsOut] = (GameObject)Instantiate (roadOut, new Vector3 (roadOutPositionX, .1f, roadOutPositionY),  Quaternion.identity);
			roadsOut[countedRoadsOut].transform.eulerAngles = new Vector3(transform.eulerAngles.x,0f, transform.eulerAngles.z);
			countedRoadsOut++;
		}
	}
	void SetTrafficLights(){
		if (Li >= 1) {
			trafficLightPositionX = -(ChosenWidth/2)-.25f;
			trafficLightPositionY = (((Lo - Li) / 2) + Li / 2)+.25f;
			trafficLights[countedTrafficLights] = (GameObject)Instantiate (trafficLight, new Vector3 (trafficLightPositionX, .6f, trafficLightPositionY), Quaternion.identity);
			trafficLights[countedTrafficLights].transform.eulerAngles = new Vector3 (transform.eulerAngles.x, 90f, transform.eulerAngles.z);
			trafficLights[countedTrafficLights].transform.localScale = new Vector3 (Li+.5f, 1f, .5f);
			trafficLights[countedTrafficLights].name = "LeftTrafficLights";
			countedTrafficLights++;
		}
		if (Ti >= 1) {
			trafficLightPositionX = (((To - Ti) / 2) + Ti / 2)+.25f;
			trafficLightPositionY = (ChosenHeight / 2) + .25f;
			trafficLights[countedTrafficLights] = (GameObject)Instantiate (trafficLight, new Vector3 (trafficLightPositionX, .6f, trafficLightPositionY), Quaternion.identity);
			trafficLights[countedTrafficLights].transform.eulerAngles = new Vector3 (transform.eulerAngles.x, 180f, transform.eulerAngles.z);
			trafficLights[countedTrafficLights].transform.localScale = new Vector3 (Ti+.5f, 1f, .5f);
			trafficLights[countedTrafficLights].name = "TopTrafficLights";
			countedTrafficLights++;
		}
		if (Ri >= 1) {
			trafficLightPositionX = (ChosenWidth/2)+.25f;
			trafficLightPositionY = -(((Ro - Ri) / 2) + Ri / 2)-.25f;
			trafficLights[countedTrafficLights] = (GameObject)Instantiate (trafficLight, new Vector3 (trafficLightPositionX, .6f, trafficLightPositionY), Quaternion.identity);
			trafficLights[countedTrafficLights].transform.eulerAngles = new Vector3 (transform.eulerAngles.x, 270f, transform.eulerAngles.z);
			trafficLights[countedTrafficLights].transform.localScale = new Vector3 (Ri+.5f, 1f, .5f);
			trafficLights[countedTrafficLights].name = "RightTrafficLights";
			countedTrafficLights++;
		}
		if (Bi >= 1) {
			trafficLightPositionX = -(((Bo - Bi) / 2) + Bi / 2)-.25f;
			trafficLightPositionY = -(ChosenHeight / 2) -.25f;
			trafficLights[countedTrafficLights] = (GameObject)Instantiate (trafficLight, new Vector3 (trafficLightPositionX, .6f, trafficLightPositionY), Quaternion.identity);
			trafficLights[countedTrafficLights].transform.eulerAngles = new Vector3 (transform.eulerAngles.x, 0f, transform.eulerAngles.z);
			trafficLights[countedTrafficLights].transform.localScale = new Vector3 (Bi+.5f, 1f, .5f);
			trafficLights[countedTrafficLights].name = "BottomTrafficLights";
			countedTrafficLights++;
		}
	}

	void makeObjectsChildOfJunction(){
		for (int i = 0; i < trafficLights.Length; i++) {
			trafficLights[i].transform.parent = this.transform;
		}
		for (int i = 0; i < islands.Length; i++) {
			islands[i].transform.parent = this.transform;
		}
		for (int i = 0; i < cornerPaths.Length; i++) {
			cornerPaths[i].transform.parent = this.transform;
		}
		for (int i = 0; i < roadsIn.Length; i++) {
			roadsIn[i].transform.parent = this.transform;
		}
		for (int i = 0; i < roadsOut.Length; i++) {
			roadsOut[i].transform.parent = this.transform;
		}
	}
}
