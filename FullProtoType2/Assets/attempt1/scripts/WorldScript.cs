﻿using UnityEngine;
using System.Collections;

public class WorldScript : MonoBehaviour {

	// Use this for initialization
	public GameObject junction;
	private JunctionBuilder jbScript;
	public int li, lo, ti, to, ri, ro, bi, bo;
	public GameObject Network;
	private NetworkTrafficLights networkScript;

	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {

	}

	public void CreateJunction(int Li,int Lo,int Ti,int To,int Ri,int Ro,int Bi,int Bo){
		junction = (GameObject)Instantiate(junction, new Vector3(0f, .1f, 0f), Quaternion.identity);
		jbScript = junction.GetComponent<JunctionBuilder> ();
		jbScript.BuildJunction (Li, Lo, Ti, To, Ri, Ro, Bi, Bo);
	}

	void DevelopNetwork(){
		Network = (GameObject)Instantiate (Network, new Vector3 (0, 20, 0), Quaternion.identity);
		Network.name = "network";
		networkScript = Network.GetComponent<NetworkTrafficLights> ();
	}
}
