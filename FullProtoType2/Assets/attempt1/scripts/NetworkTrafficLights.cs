﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NetworkTrafficLights : MonoBehaviour {
	public GameObject scenarioPrefab;
	public GameObject[] trafficLights;
	public GameObject trafficLightsLeft, trafficLightsTop, trafficLightsRight, trafficLightsBottom;
	public List<GameObject> scenarios = new List<GameObject> ();
	private float timeTilNextScenario = 10;
	private int getNextScenario = 0;
	public float timeWaited = 0;

	//light scenarios

	// Use this for initialization
	void Start () {
		getLights ();
		for (int i = 0; i < 5; i++) {
			scenarios.Add ((GameObject)Instantiate (scenarioPrefab, new Vector3 (0f, 20f, 0f), Quaternion.identity));
		}
		MakeLightsChild ();
	}
	
	// Update is called once per frame
	void Update () {
		waitForScenarioChange ();

	}

	void getLights(){
		trafficLights = GameObject.FindGameObjectsWithTag ("TrafficLights");
		for (int i = 0; i < trafficLights.Length; i++) {
			if (trafficLights[i].name.Equals ("LeftTrafficLights")) {
				trafficLightsLeft = trafficLights [i];
			}else if (trafficLights[i].name.Equals ("TopTrafficLights")) {
				trafficLightsTop = trafficLights [i];
			}else if (trafficLights[i].name.Equals ("RightTrafficLights")) {
				trafficLightsRight = trafficLights [i];
			} else if (trafficLights[i].name.Equals ("BottomTrafficLights")) {
				trafficLightsBottom = trafficLights [i];
			}
		}
	}
	void waitForScenarioChange(){
		timeWaited += Time.deltaTime;
		if (timeWaited >= timeTilNextScenario) {
			StartCoroutine (ChangeLightScenario ());
			timeWaited = 0;
		}
	}	

	IEnumerator ChangeLightScenario()
	{
		LightScenario ls = scenarios [getNextScenario].GetComponent<LightScenario> ();
		turnLightsRed ();
		yield return new WaitForSeconds(ls.breakTime);
		setLights (ls);
		if (getNextScenario < scenarios.Count - 1) {
			getNextScenario++;
		} else {
			getNextScenario = 0;
		}
		timeTilNextScenario = ls.time;
	}

	void setLights(LightScenario ls){
		TrafficLightsBehaviour tlb;
		if (ls.left) {
			if (trafficLightsLeft.name.Equals ("LeftTrafficLights")) {
				tlb = trafficLightsLeft.GetComponent<TrafficLightsBehaviour> ();
				tlb.makeGreen ();
			}
		} else {
			if (trafficLightsLeft.name.Equals ("LeftTrafficLights")) {
				tlb = trafficLightsLeft.GetComponent<TrafficLightsBehaviour> ();
				tlb.makeRed ();
			}
		}if (ls.top) {
			if(trafficLightsTop.name.Equals("TopTrafficLights")){
				tlb = trafficLightsTop.GetComponent<TrafficLightsBehaviour> ();
				tlb.makeGreen ();
			}
		} else {
			if (trafficLightsTop.name.Equals("TopTrafficLights")){
				tlb = trafficLightsTop.GetComponent<TrafficLightsBehaviour> ();
				tlb.makeRed ();
			}
		} if (ls.right) {
			if(trafficLightsRight.name.Equals("RightTrafficLights")){
				tlb = trafficLightsRight.GetComponent<TrafficLightsBehaviour> ();
				tlb.makeGreen ();
			}
		} else {
			if (trafficLightsRight.name.Equals("RightTrafficLights")){
				tlb = trafficLightsRight.GetComponent<TrafficLightsBehaviour> ();
				tlb.makeRed ();
			}
		} if (ls.bottom) {
			if(trafficLightsBottom.name.Equals("BottomTrafficLights")){
				tlb = trafficLightsBottom.GetComponent<TrafficLightsBehaviour> ();
				tlb.makeGreen ();
			}
		} else {
			if (trafficLightsBottom.name.Equals("BottomTrafficLights")){
				tlb = trafficLightsBottom.GetComponent<TrafficLightsBehaviour> ();
				tlb.makeRed ();
			}
		}
	}
	void MakeLightsChild(){
		foreach(GameObject sc in scenarios) {
			sc.transform.parent = this.transform;
		}
	}
	void turnLightsRed(){
		TrafficLightsBehaviour tlb;
		foreach (GameObject tl in trafficLights) {
			tlb = tl.GetComponent<TrafficLightsBehaviour> ();
			tlb.makeRed ();
		}
	}
}
