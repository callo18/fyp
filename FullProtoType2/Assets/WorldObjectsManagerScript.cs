﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WorldObjectsManagerScript : MonoBehaviour {

	private WorldBuildingManager worldBuildingMan;
	private EditingLanesManager editingLanesMan;
	private PreGameSettingManager gameSettingsMan;
	private SimulationManager simMan;
	private PostGameReportManager postGameReportMan;

	public JunctionPiece junctionPiece;

	public GameObject NLights,ELights,SLights,WLights;

	public List<GameObject> NSpawners,ESpawners,SSpawners,WSpawners;

	public List<GameObject> NCollectors,ECollectors,SCollectors,WCollectors;

	public List<GameObject> NDecisionPointers,EDecisionPointers,SDecisionPointers,WDecisionPointers;

	public List<GameObject> NDestinationPointers,EDestinationPointers,SDestinationPointers,WDestinationPointers;

	public List<GameObject> AllCars;

	void Start () {

		// INITIALIZING GAME MANAGAGERS

		worldBuildingMan = GameObject.Find ("WorldBuildingManager").GetComponent<WorldBuildingManager> ();

		editingLanesMan = GameObject.Find ("EditingLanesManager").GetComponent<EditingLanesManager> ();

		gameSettingsMan = GameObject.Find ("GameSettingsManager").GetComponent<PreGameSettingManager> ();

		simMan = GameObject.Find ("SimulationManager").GetComponent<SimulationManager> ();

		postGameReportMan = GameObject.Find ("PostGameReportManager").GetComponent<PostGameReportManager> ();
	}

	public void setUpGame (){
		
		collectJunction ();

		editingLanesMan.setUp ();

		gameSettingsMan.setUp ();

		simMan.setUp ();
	}

	public void prepareForNewGame(){
		
		worldBuildingMan.reset ();

		editingLanesMan.resetLists ();

		gameSettingsMan.resetLists ();

		resetLists ();
	}

	public void collectJunction(){

		junctionPiece = GameObject.Find ("Junction(Clone)").GetComponent<JunctionPiece> ();

		NLights = junctionPiece.NLights;
		ELights = junctionPiece.ELights;
		SLights = junctionPiece.SLights;
		WLights = junctionPiece.WLights;

		NSpawners = junctionPiece.NSpawners;
		ESpawners = junctionPiece.ESpawners;
		SSpawners = junctionPiece.SSpawners;
		WSpawners = junctionPiece.WSpawners;

		NCollectors = junctionPiece.NCollectors;
		ECollectors = junctionPiece.ECollectors;
		SCollectors = junctionPiece.SCollectors;
		WCollectors = junctionPiece.WCollectors;

		NDecisionPointers = junctionPiece.NDecisionPointers;
		EDecisionPointers = junctionPiece.EDecisionPointers;
		SDecisionPointers = junctionPiece.SDecisionPointers;
		WDecisionPointers = junctionPiece.WDecisionPointers;

		NDestinationPointers = junctionPiece.NDestinationPointers;
		EDestinationPointers = junctionPiece.EDestinationPointers;
		SDestinationPointers = junctionPiece.SDestinationPointers;
		WDestinationPointers = junctionPiece.WDestinationPointers;
	}

	public void resetLists(){
		
		NLights = null;
		ELights = null;
		SLights = null;
		WLights = null;

		NSpawners.Clear();
		ESpawners.Clear();
		SSpawners.Clear();
		WSpawners.Clear();

		NCollectors.Clear();
		ECollectors.Clear();
		SCollectors.Clear();
		WCollectors.Clear();

		NDecisionPointers.Clear();
		EDecisionPointers.Clear();
		SDecisionPointers.Clear();
		WDecisionPointers.Clear();

		NDestinationPointers.Clear();
		EDestinationPointers.Clear();
		SDestinationPointers.Clear();
		WDestinationPointers.Clear();
	}

	public void addCar(GameObject car){

		AllCars.Add (car);
	}

	public void deleteCars(){

		foreach (GameObject c in AllCars) {
			if (c != null) {
				Destroy (c);
			}
		}
		AllCars.Clear ();
	}
}
